# Ovnitrap

Bem-vindo(a) à documentação do **Ovnitrap**! Aqui você conhecerá como ocorreu a execução do projeto, os artefatos gerados, tal como suas principais funcionalidades.

## Sobre o Projeto

O Ovnitrap é um projeto integrador de 5 engenharias (eletrônica, software, energia, automotiva e aeroespacial) que visa combater o avanço do mosquito da dengue. Estamos focados em:

- Identificar locais com foco do mosquito;
- Impedir que larvas se tornem novos mosquitos;
- Auxiliar autoridades e organizações no monitoramento do _Aedes aegypti_;
- Permitir estudos com larvas capturadas.

## Principais Recursos

- **Feature 1:** Armadilha que captura as larvas do mosquito;
- **Feature 2:** Aplicativo que monitora o estado da armadilha e de atividade das larvas capturadas;
- **Feature 3:** Geração de relatório com dados de atividade e geolocalização.

© 2024 Ovnitrap. Feito com ❤️

## Execução local

1. Clone o repositório

1. Instale as dependências (Docker)

1. Em um terminal, execute o container com o seguinte comando:

   `docker compose up --build`

1. Acesse o endereço em um navegador:

   > [http://localhost:8000](http://localhost:8000)

### Adição de novos documentos

1. Adicionar novo documento na pasta `./docs`

1. Adicionar o nome do arquivo no `mkdocs.yml`

   ```
   nav:
     - 'Home': 'index.md'
     - 'Semestres Anteriores': 'anteriores.md'
   ```

### Acessar documentação

[Link do Gitlab Pages](https://ovnitrap-relatorio-fga-pi2-semestre-2024-1-g1-c48d33da1876a99c9.gitlab.io)
