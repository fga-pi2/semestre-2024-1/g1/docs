# Aspectos de gerenciamento do projeto

## Termo de abertura do projeto

### Informações do Projeto

- **Nome do Projeto:** Ovnitrap
- **Gerente do Projeto:** Gabriel Bacon Carvalho
- **Data de Início Prevista:** 03/05/2024
- **Data de Término Prevista:** 10/07/2024

### Equipe do projeto

<figcaption style="text-align: center">
    <b>Tabela 1: Informações sobre a equipe do projeto</b>
</figcaption>
<center>

| Integrante                         | Matrícula |          Curso          |         Papel          |
| :--------------------------------- | :-------: | :---------------------: | :--------------------: |
| Gabriel Bacon Carvalho             | 180136569 |  Engenharia Eletrônica  |   Coordenador Geral    |
| Luís Guilherme Gaboardi Lins       | 180022962 | Engenharia de Software  |  Diretor de Qualidade  |
| Luan Vasco Cavalcante              | 190111836 | Engenharia de Software  |  Diretor de Software   |
| Jhéssica Isabel Coelho Souza       | 170146031 |  Engenharia Eletrônica  | Diretora de Eletrônica |
| Miguel Meira Wichoski              | 200064096 |  Engenharia de Energia  |   Diretor de Energia   |
| Carlos Eduardo Vaz Ferreira        | 190139439 | Engenharia Aeroespacial |  Diretor de Estrutura  |
| Luiz Guilherme Palhares Pettengill | 150138202 | Engenharia de Software  |     Desenvolvedor      |
| Kayro César Silva Machado          | 170107426 | Engenharia de Software  |     Desenvolvedor      |
| Lucas Gabriel Sousa Camargo Paiva  | 190112123 | Engenharia de Software  |     Desenvolvedor      |
| Victor Yukio Cavalcanti Miki       | 180068229 | Engenharia de Software  |     Desenvolvedor      |

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Descrição do Projeto

O projeto será desenvolvido por uma equipe multidisciplinar conforme consta na Tabela 1, validando-se da visão holística de diversas Engenharias, com intuito de solucionar um problema que acomete a população brasileira de forma persistente. Nesse sentido, essa iniciativa consiste em desenvolver uma armadilha, chamada de Ovnitrap, para captura de mosquitos na fase larval e através disso realizar o monitoramento da atividade de mosquitos na região por intermédio de de um aplicativo para celular com o objetivo de fornecer dados relevantes sobre a atividade do mosquito na região para autoridades de forma que essas possam tomar ações com objetivo de diminuir o número de casos da doença nas regiões mais afetadas.

Esses dispositivos são projetados para atrair mosquitos fêmeas, que depositam seus ovos em recipientes contendo água. Uma vez que os ovos são depositados, a armadilha permite o monitoramento dessas na fase de larvas e impede as larvas de se tornarem mosquitos, ajudando no controle da população de mosquitos, especialmente do _Aedes aegypti_ que transmite doenças como a dengue, a zika e a chikungunya.

### Justificativa do Projeto

A importância do projeto ocorre em decorrência do fato da dengue representar uma ameaça a saúde das pessoas de forma mundial, pois é uma doença endemica em 100 países [(1)](apendice1.md#referencias). Ademias, os mosquitos estão associados a 390 milhões infecções anuais por dengue no mundo [(1)](apendice1.md#referencias), causando dano à vida humana e à economia de países como o Brasil [(2)](apendice1.md#referencias). No Distrito Federal (DF), essa situação também se demonstra alarmante, devido ao alto número de casos suspeitos da doença no ano de 2024, totalizando 70.083, dos quais 67.897 eram prováveis e 97,7% desses são referentes a residentes do DF, representando um aumento de 1.303,9% na quantidade de casos prováveis em residentes do DF quando comparado ao mesmo período no ano de 2023 [(3)](apendice1.md#referencias). Ante o exposto, o projeto 
Ovnitrap pode auxiliar a sociedade no combate à doença por meio do monitoramento da população de mosquitos nas regiões em que for utilizado, fornecendo assim uma ferramenta efetiva de monitoramento às autoridades e organizações. Outras soluções existem porém possuem dificuldades e é importante ter alternativas diferentes de combate.

### Objetivo do Projeto

Garantir a implementação bem-sucedida de uma armadilha para mosquitos da dengue na fase de larva, visando contribuir para o monitoramento eficaz e sustentável desses insetos. O projeto visa desenvolver um sistema automatizado e interdisciplinar, unindo conhecimentos de 5 Engenharias da Universidade de Brasília, Software, Automotiva, Eletrônica, Aeroespacial e Energia, para que seja capaz de capturar, monitorar e eliminar as larvas do mosquito _Aedes aegypti_, possibilitando a identificação de focos, promovendo informações que ajudem no combate ao mosquito. Isso auxiliará na redução dos índices de infestação, por meio de informações que poderão ser usados para realizar ações de combate ao _Aedes aegypti_, de forma mais eficaz, com foco em regiões que possuam maior quantidade de mosquitos, o que por conseguinte minimizará o risco de transmissão da dengue.

### Escopo do Projeto

Uma estratégia inovadora para combater mosquitos transmissores de doenças envolve o uso de armadilhas específicas que capturam larvas. Essas armadilhas atraem mosquitos fêmeas para depositar seus ovos em um ambiente controlado, evitando que se desenvolvam em mosquitos adultos, reduzindo assim a população desses insetos.

Para facilitar o acompanhamento dessas armadilhas, será criado um aplicativo que monitora sua atividade e a presença de larvas. Com ele, é possível verificar se as armadilhas estão funcionando corretamente e capturando larvas. Além disso, o aplicativo gera relatórios detalhados com informações sobre a localização das armadilhas e os dados de atividade, permitindo uma avaliação eficaz da estratégia de controle de mosquitos.

### Stakeholders Principais

- Pesquisadores - realizar estudos sobre o mosquito e as doenças transmitidas por ele, como a dengue
- Inspetores de Saúde Local - garantir a segurança e o bem-estar da comunidade em sua área de atuação
- Autoridades de Saúde Pública - responsabilidade de implementar políticas e programas de controle de doenças
- Comunidade Local - preocupada com sua própria segurança e saúde, bem como com a de seus entes queridos

## Orçamento

<figcaption style="text-align: center">
    <b>Tabela 2: Orçamento do projeto de eletrônica</b>
</figcaption>

<center>

| Eletrônicos                                                  | Quantidade | Preço Unitário | Preço Total |
| ------------------------------------------------------------ | ---------- | -------------- | ----------- |
| Raspberry pi 3b+                                             | 1          | R$378.00       | R$378.00    |
| Cartão De Memória 32gb U3                                    | 1          | R$66.00        | R$66.00     |
| Webcam C925e                                                 | 1          | R$545.00       | R$545.00    |
| Relé Temporizador Digital                                    | 1          | R$30.00        | R$30.00     |
| 2x Módulo Relé Relay 3v 2 Canais                             | 1          | R$19.00        | R$19.00     |
| Válvula Solenóide 2/2 Vias N. Fechada 3/4 Água/óleo/ar 24v   | 1          | R$190.00       | R$190.00    |
| Mini Bomba D'água 22w 5m 800l/h 24v Dc                       | 1          | R$119.00       | R$119.00    |
| Sensor De Distância Ultrassom Hc-sr04                        | 2          | R$18.00        | R$36.00     |
| 30x Módulo Led Lilypad Branco 3-5v Para Arduino Lilypad      | 1          | R$50.00        | R$50.00     |
| 20 Un Led 5mm Alto Brilho Ultravioleta Uv + Resistor 12v     | 1          | R$15.00        | R$15.00     |
| Sensor de temperatura Ds18b20                                | 3          | R$17.00        | R$51.00     |
| Ina3221 - Módulo Sensor De Corrente E Tensão Dc 3 Canais I2c | 1          | R$29.00        | R$29.00     |
| Sim808 Módulo Gsm Gprs Gps Placa De Desenvolvimento Ipx Sma  | 1          | R$170.00       | R$170.00    |
| Módulo Gps                                                   | 1          | R$50.00        | R$50.00     |
| Módulo Gprs Gsm Arduino Sim800l                              | 1          | R$40.00        | R$40.00     |
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Tabela 3: Orçamento do projeto de energia</b>
</figcaption>

| Energia                            | Quantidade | Preço Unitário | Preço Total |
| ---------------------------------- | ---------- | -------------- | ----------- |
| Bateria 12v 1,3ah                  | 2          | R$49.50        | R$99.00     |
| Regulador De Tensão de 24V para 5V | 1          | R$61.00        | R$61.00     |
| Connector wago até 4mm² 10 pc      | 1          | R$50.00        | R$50.00     |
| Cabo epr 0,5mm (pegar na FGA)      | X          | X              | X           |
| Placa ufv (pegar na FGA)           | X          | X              | X           | 
| Controlador de carga solar         | 1          | R$45.00        | R$45.00     |
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Tabela 4: Orçamento do projeto de estrutura</b>
</figcaption>

| Estrutura                                       | Quantidade | Preço Unitário | Preço Total |
| ----------------------------------------------- | ---------- | -------------- | ----------- |
| Balde plástico retângular de 20 litros          | 1          | R$28.90        | R$28.90     |
| Cap pvc 100mm                                   | 1          | R$12.90        | R$12.90     |
| Cano pvc 100mm                                  | 1          | R$25.00        | R$25.00     |
| Mangueira 1/2'' 1 metro                         | 1          | R$4.90         | R$4.90      |
| Mangueira 1/4'' 1 metro                         | 1          | R$2.90         | R$2.90      |
| Perfil de alumínio                              | 3          | R$20.00        | R$20.00     |
| Madeira                                         | 1          | R$30.00        | R$30.00     |
| Cola quente                                     | 4          | R$1.50         | R$6.00      |
| Tinta Spray                                     | 1          | R$17.00        | R$17.00     |
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Tabela 5: Orçamento geral do projeto Ovnitrap</b>
</figcaption>

| Total (comprar) | R$1,083.02 |
| --------------- | ---------- |
| Por membro      | R$108.30   |
| Total projeto   | R$2,190.60 |

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

> Observação 1: os valores substituídos por 'X' no momento não são possíveis determinar.

> Observação 2: Houve a troca do material que seria utilizado na estrutura da placa solar, onde no PC1 foi indicado o uso de impressão 3D PLA, e no PC2 ocorreu a mudança para perfis de alumínio, com a finalidade de reduzir significamente os custos do projeto final.

## **Repositórios**

[Documentação](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/docs)

[Frontend](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/frontend)

[Backend](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/backend)

[Visão Computacional](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/visao-computacional)


## Referências

> 1. PFIZER. 6 mosquito diseases that can be deadly. Disponível em: https://www.pfizer.com/news/articles/6_mosquito_diseases_that_can_be_deadly#.Yweo2_DM88I.link. Acesso em: 24 de abril de 2024.

> 2. CIBELLE BOUÇAS. Epidemia de dengue, chikungunya e zica terá impacto de R$ 20 bi na economia, diz estudo. Valor Econômico. Disponível em: <https://valor.globo.com/brasil/noticia/2024/03/04/epidemia-de-dengue-chikungunya-e-zica-tera-impacto-de-r-20-bi-na-economia-diz-estudo.ghtml>. Acesso em: 31 de maio de 2024.

> 3. Monitoramento dos casos de dengue até a Semana Epidemiológica 06 de 2024 no Distrito Federal. Disponível em: <https://www.saude.df.gov.br/documents/37101/0/06_BOLETIM_SEMANAL_DENGUE_SE_06_+DF+2024.pdf/f4f0ca52-82f0-a505-264f-fe7052f5cfe9?t=1707944555411#:~:text=Em%202024%2C%20at%C3%A9%20a%20SE>. Acesso em: 22 de maio de 2024.

## Histórico de versões

| Versão | Alteração                               | Data       | Autor               |
| ------ | --------------------------------------- | ---------- | ------------------- |
| 1.0    | Criação do documento                    | 26/04/2024 | Gabriel e Luís Lins |
| 1.1    | Complemento do documento                | 27/04/2024 | Victor Yukio        |
| 1.2    | Anexa gerenciamento de risco            | 29/04/2024 | Gabriel e Luís Lins |
| 1.3    | Adição de contexto sobre a dengue no DF | 22/05/2024 | Lucas Gabriel       |
| 1.4    | Correções gerais no documento           | 31/05/2024 | Lucas Gabriel       |
| 1.5    | Correções do orçamento                  | 31/05/2024 | Carlos Vaz          |
