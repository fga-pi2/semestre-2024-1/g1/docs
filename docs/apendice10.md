# Resultados da Pesquisa sobre Alternativas para Combate à Dengue

Para realização da pesquisa foi criado e enviado um formulário de pesquisa a respeito do combate ao mosquito da dengue e enviado a familiares, amigos próximos e grupos de mensagem gerais da região do DF e entorno. O formulário pode ser acessado em: [_Google Forms_](https://docs.google.com/forms/d/e/1FAIpQLSc9urPnUcTRnwX0OI_G1qFIJQFFtgc5QV5ypfGD_FyZOXq65w/viewform?usp=sf_link)


## Seção 1: Práticas Atuais

O objetivo deste formulário é coletar informações e sugestões sobre estratégias eficazes para o combate à dengue. Queremos entender melhor as práticas atuais, identificar áreas de melhoria e considerar novas abordagens que possam ser implementadas em nossa comunidade. Mas antes de iniciar a pesquisa é preciso saber o que são ovitrampas.

Ovitrampas são dispositivos usados para monitorar e controlar a população de mosquitos Aedes aegypti, o principal transmissor da dengue. Elas consistem em recipientes com água e uma palheta ou substrato onde as fêmeas depositam seus ovos. As ovitrampas permitem a captura de ovos e larvas, ajudando a reduzir a população de mosquitos e a monitorar a presença do vetor.

### 1. Vocês sabia oque são ovitrampas?

<figcaption style="text-align: center">
    <b>Figura 1: Pergunta 1</b>
</figcaption>

<center>

![Pergunta 1](assets/images/forms/p1.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### 2. Você adota alguma medida preventiva contra a dengue?

<figcaption style="text-align: center">
    <b>Figura 2: Pergunta 2</b>
</figcaption>

<center>

![Pergunta 2](assets/images/forms/p2.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>


## Seção 2: Sugestões e medidas protetivas

### 3. Se sim, quais medidas você adota regularmente?

<figcaption style="text-align: center">
    <b>Figura 3: Pergunta 3</b>
</figcaption>

<center>

![Pergunta 3](assets/images/forms/p3.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Seção 3: Conclusão

Comparação: Fumacê vs. Ovitrampas O fumacê é um método tradicional de combate aos mosquitos, que envolve a pulverização de inseticidas em áreas externas para matar os mosquitos adultos. No entanto, este método tem suas limitações e desafios:

Eficácia Temporária: O fumacê mata os mosquitos adultos, mas não afeta os ovos e larvas, o que pode resultar em um rápido retorno da população de mosquitos.
Impacto Ambiental: O uso de inseticidas pode afetar outras espécies de insetos e contaminar o meio ambiente.
Saúde Humana: A exposição frequente aos inseticidas pode ter efeitos adversos à saúde das pessoas.

### 4. Você acredita que o uso de ovitrampas pode ser eficaz no combate à dengue e usaria no lugar opções como fumacê?

<figcaption style="text-align: center">
    <b>Figura 4: Pergunta 4</b>
</figcaption>

<center>

![Pergunta 4](assets/images/forms/p4.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### 5. Você acredita que a sua comunidade está bem informada sobre a dengue e suas formas de prevenção?

<figcaption style="text-align: center">
    <b>Figura 5: Pergunta 5</b>
</figcaption>

<center>

![Pergunta 5](assets/images/forms/p5.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Conclusões

A pesquisa sobre alternativas para combate à dengue, focada no uso de ovitrampas, revelou _insights_ importantes que destacam as vantagens das ovitrampas como uma estratégia eficaz e sustentável.

Os dados coletados mostram que uma grande maioria dos participantes acredita que as ovitrampas podem ser eficazes no combate à dengue. A principal razão é que as ovitrampas interrompem o ciclo de vida dos mosquitos ao capturar e destruir ovos, o que ajuda a controlar a população de mosquitos a longo prazo.

Um número significativo de respondentes expressou disposição para utilizar ovitrampas em suas casas e comunidades. Esta disposição sugere que, com a devida informação e apoio, a implementação das ovitrampas poderia ser bem-sucedida e amplamente aceita.

A pesquisa revelou também a necessidade de envolver a comunidade com mais informações a respeito da dengue, algo que nosso projeto ajuda a divulgar.

Com base nas respostas coletadas, é evidente que as ovitrampas representam uma alternativa promissora para o combate à dengue. Elas oferecem uma solução sustentável, eficaz e bem aceita pela comunidade. A implementação bem-sucedida dependerá da promoção de campanhas educativas e do apoio contínuo à comunidade. A adoção de ovitrampas, em conjunto com outras medidas preventivas, pode contribuir significativamente para a redução da incidência de dengue e a proteção da saúde pública.

## Histórico de versões

| Versão | Alteração                                | Data       | Autor        |
| ------ | ---------------------------------------- | ---------- | ------------ |
| 1.0    | Criação do documento                     | 31/05/2024 | Victor Yukio |
| 1.1    | Adicionar link do forms                  | 31/05/2024 | Victor Yukio |
| 1.2    | centralizar imagem e pequenas correções. | 31/05/2024 | Victor Yukio |