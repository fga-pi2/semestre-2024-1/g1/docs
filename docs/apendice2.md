# Diagramas de Eletrônica

## Esquemático Eletrônico

<center>

![Esquemático Eletrônico](assets/images/esquematico-eletronico-completo.png)

</center>

<figcaption style="text-align: center">
    <p>Figura 1: Esquemático eletrônico</p>
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Fluxograma de Firmware da Raspberry

<center>

![Fluxograma de Firmware da Raspberry](assets/images/fluxograma-raspberry.jpeg)

</center>

<figcaption style="text-align: center">
    <p>Figura 2: Fluxograma de Firmware da Raspberry</p>
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Histórico de versões

| Versão | Alteração                                 | Data       | Autor               |
| ------ | ----------------------------------------- | ---------- | ------------------- |
| 1.0    | Criação do documento                      | 26/04/2024 | Gabriel e Jhessica  |
| 1.1    | Anexa fluxograma de firmware da Raspberry | 02/06/2024 | Gabriel e Luís Lins |
