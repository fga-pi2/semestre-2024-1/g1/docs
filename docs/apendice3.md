# Diagrama de casos de uso

É uma ferramenta importante na modelagem de sistemas de software que descreve as interações entre um sistema de seus atores.

A representação gráfica do diagrama de casos de uso geralmente é feita em UML e podem ter ícones que representam atores, sistemas externos e casos de uso.

## Componentes

### Atores

Um ator é qualquer entidade que interage com o sistema e é representado geralmente por um boneco. Ex: Usuário humano, sistema externo e etc...

<figcaption style="text-align: center">
    <b>Figura 1: Ator</b>
</figcaption>

<center>

![Ator](assets/images/ator.png){: width="300" height="200"}

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Casos de uso

Representa uma função ou recurso que um sistema de software faz. Geralmente representado por uma elipse e por um verbo no infinitivo.

<figcaption style="text-align: center">
    <b>Figura 2: Casos de uso</b>
</figcaption>

<center>

![Casos de uso](assets/images/elipse.png){: width="300" height="200"}

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Relações

Representadas por linhas, as relações acontecem entre autores e casos de usos.

<figcaption style="text-align: center">
    <b>Figura 3: Relações</b>
</figcaption>

<center>

![Relações](assets/images/relacionamento.png){: width="300" height="200"}

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Extensões e Inclusões

Extensões e inclusões indicam que algumas funcionalidades podem acontecer dependendo de outras.

Quando um caso de uso inclui outro caso de uso, o segundo é sempre chamado quando o primeiro for chamado.

Já quando existe uma extensão de um caso de uso, o segundo pode acontecer ou não.

<figcaption style="text-align: center">
    <b>Figura 4: Extensões e Inclusões</b>
</figcaption>

<center>

![Extensões e Inclusões](assets/images/extensao-inclusao.png){: width="300" height="200"}

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Diagrama de casos de uso

O diagrama de casos de uso para o projeto está na figura abaixo :

<figcaption style="text-align: center">
    <b>Figura 5: Diagrama de casos de uso</b>
</figcaption>

<center>

![Diagrama de casos de uso](assets/images/diagrama-casos-de-uso.png){: width="1000" height="1000"}

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Ferramentas utilizadas

> Draw.io, Figma.

## Histórico de versões

| Versão | Alteração                           | Data       | Autor         |
| ------ | ----------------------------------- | ---------- | ------------- |
| 1.0    | Criação do documento com a versão 1 | 24/04/2024 | Luan Vasco    |
| 1.1    | Ajustes no diagrama de caso de uso  | 26/04/2024 | Lucas Gabriel |
