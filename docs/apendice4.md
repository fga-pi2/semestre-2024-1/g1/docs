# Orçamento do projeto

Segue abaixo o orçamento com a expectativa de custo dos componentes:

<figcaption style="text-align: center">
    <b>Tabela 1: orçamento com a expectativa de custo dos componentes</b>
</figcaption>
<center>

| Eletrônicos | Componente                             | Quantidade | Preço Unitário | Preço total |
| ----------- | -------------------------------------- | ---------- | -------------- | ----------- |
|             | Raspberry pi 3b+                       | 1          | R$378.00       | R$378.00    |
|             | Cartão De Memória 32gb U3              | 1          | R$66.00        | R$66.00     |
|             | Webcam C925e                           | 1          | R$545.00       | R$545.00    |
|             | Relé Temporizador Digital              | 1          | R$30.00        | R$30.00     |
|             | 2x Módulo Relé Relay 3v 2 Canais       | 1          | R$19.00        | R$19.00     |
|             | Válvula Solenóide 3/4 24v              | 1          | R$190.00       | R$190.00    |
|             | Mini Bomba D'água 22w 5m 800l/h 24v Dc | 1          | R$119.00       | R$119.00    |
|             | Sensor Ultrassom Hc-sr04               | 2          | R$18.00        | R$36.00     |
|             | 30x Módulos Led 3-5v                   | 1          | R$50.00        | R$50.00     |
|             | 20x Led 5mm UV                         | 1          | R$15.00        | R$15.00     |
|             | Sensor de temperatura Ds18b20          | 3          | R$17.00        | R$51.00     |
|             | Ina3221 Sensor Tensão Dc 3 Canais      | 1          | R$29.00        | R$29.00     |
|             | Sim808 Módulo Gsm Gprs Gps             | 1          | R$170.00       | R$170.00    |
| Energia     |                                        |            |                |             |
|             | Bateria 12v 1,3ah                      | 2          | R$49.50        | R$99.00     |
|             | Regulador De Tensão 24V para 5V        | 1          | R$61.00        | R$61.00     |
|             | connector wago até 4mm² 10 pc          | 1          | R$50.00        | R$50.00     |
|             | cabo epr 0,5mm                         |            | x              |             |
|             | placa ufv                              | 1          | x              |             |
|             | controlador de carga solar             | 1          | R$45.00        | R$45.00     |
| Estrutura   |                                        |            |                |             |
|             | Balde plástico retângular de 20 litros | 1          | R$28.90        | R$28.90     |
|             | Cap pvc 100mm                          | 1          | R$12.90        | R$12.90     |
|             | Cano pvc 100mm                         | 1          | R$25.00        | R$25.00     |
|             | Mangueira 1/2'' 1 metro                | 1          | R$4.90         | R$4.90      |
|             | Mangueira 1/4'' 1 metro                | 1          | R$2.90         | R$2.90      |
|             | Perfil de alumínio                     | 3          | R$20.00        | R$20.00     |
|             | Madeira                                | 1          | R$30.00        | R$30.00     |
|             | Cola quente                            | 4          | R$1.50         | R$6.00      |
|             | Tinta Spray                            | 1          | R$17.00        | R$17.00     |
| Total       |                                        |            |                | R$2,190.60  |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

x : Os dados com x são componentes que ainda não podem ser incluídos pois até o presente momento não foi possível determinar os preços.

## Histórico de Versões

| Versão | Alteração              | Data       | Autor         |
| ------ | ---------------------- | ---------- | ------------- |
| 1.0    | Criação do documento   | 27/04/2024 | Gabriel Bacon |
| 1.1    | Correções do orçamento | 31/05/2024 | Carlos Vaz    |
