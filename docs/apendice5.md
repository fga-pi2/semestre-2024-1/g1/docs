# Estrutura Analítica do Projeto

<figcaption style="text-align: center">
    <b>Figura 1: EAP</b>
</figcaption>

<center>

![EAP](assets/images/eap.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Histórico de versões

| Versão | Alteração            | Data       | Autor     |
| ------ | -------------------- | ---------- | --------- |
| 1.0    | Criação do documento | 26/04/2024 | Luís Lins |
