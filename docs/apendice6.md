# Esquemático de energia

<figcaption style="text-align: center">
    <b>Figura 1: Esquemático de Energia</b>
</figcaption>

<center>

![Esquemático de Energia](assets/images/diagrama-unifilar-1.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Histórico de versões

| Versão | Alteração            | Data       | Autor                 |
| ------ | -------------------- | ---------- | --------------------- |
| 1.0    | Criação do documento | 27/04/2024 | Miguel Meira Wichoski |
