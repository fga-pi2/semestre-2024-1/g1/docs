# Desenho Técnico dos Componentes da Estrutura

<figcaption style="text-align: center">
    <b>Figura 1: Balde da estrutura</b>
</figcaption>

<center>

![Balde da estrutura](assets/images/drawing1-balde.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Figura 2: Tampa do balde</b>
</figcaption>

<center>

![Tampa do balde](assets/images/drawing2-tampa.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Figura 3: Cano PVC superior</b>
</figcaption>

<center>

![Cano PVC superior](assets/images/drawing3-cano.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Figura 4: Cap do cano superior com a fixação</b>
</figcaption>

<center>

![Cap do cano superior com a fixação](assets/images/drawing4-cap.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Figura 5: Estrutura da placa solar</b>
</figcaption>

<center>

![Estrutura da placa solar](assets/images/drawing5-est-placa-solar.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Figura 6: Vista explodida</b>
</figcaption>

<center>

![Vista explodida](assets/images/drawing6-explode.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Figura 7: Carrinho de Metalon</b>
</figcaption>

<center>

![Carrinho de Metalon](assets/images/drawing7-carrinho.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Histórico de versões

| Versão | Alteração               | Data       | Autor      |
| ------ | ----------------------- | ---------- | ---------- |
| 1.0    | Criação do documento    | 27/04/2024 | Carlos Vaz |
| 1.1    | Atualização das imagens | 01/06/2024 | Carlos Vaz |
| 1.2    | Atualização das imagens | 11/07/2024 | Carlos Vaz |