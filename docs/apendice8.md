# Gerenciamento de Riscos do Projeto

O risco é um evento que tem a possibilidade de ocorrer no futuro e influenciar o projeto de maneira adversa (ameaça) ou favorável (oportunidade). Nesse sentido, o gerenciamento de riscos é fundamental para o sucesso de um projeto. [(1)](apendice8.md#referencias)

O gerenciamento de riscos de um projeto abrange uma série de processos, que incluem planejamento, identificação, análise, monitoramento, planejamento de respostas e implementação das respostas aos riscos no decorrer de um projeto.
O principal objetivo do gerenciamento de riscos é avaliar e gerenciar os riscos do projeto visando maximizar os eventos positivos e minimizar os eventos negativos. [(1)](apendice8.md#referencias)

## Estrutura Analítica de Riscos

Visando um melhor entendimento dos riscos associados ao projeto em questão, foi criado um diagrama de Estrutura Analítica
de Riscos, apresentado na figura 1 e descrito na Tabela 1.

<figcaption style="text-align: center">
    <b>Figura 1 - Estrutura Analítica de Riscos</b>
</figcaption>

<center>

![Estrutura Analítica de Riscos](assets/images/EAR.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Tabela 1 – Descrição da Estrutura Analítica de Riscos</b>
</figcaption>
<center>

| Tipo          | Descrição                                                                                       |
| ------------- | ----------------------------------------------------------------------------------------------- |
| Planejamento  | Riscos relacionados ao planejamento e cadência das entregas                                     |
| Controle      | Riscos relacionados ao controle de tarefas realizadas e pendentes                               |
| Comunicação   | Riscos relacionados à comunicação entre membros da equipe e professores                         |
| Requisitos    | Riscos relacionados à especificação do produto                                                  |
| Tecnologia    | Riscos relacionados às tecnologias de software escolhidas para desenvolvimento                  |
| Qualidade     | Riscos relacionados ao não atendimento das necessidades do usuário                              |
| Recursos      | Riscos relacionados aos recursos físicos e tecnológicos disponíveis                             |
| Financiamento | Riscos relacionados ao investimento inicial disponível                                          |
| Priorização   | Riscos relacionados à priorização incorreta das atividades essenciais para a entrega do projeto |
| Dependências  | Riscos associados à coordenação entre as tarefas para a entrega do projeto                      |
| Greve         | Riscos relacionados ao cenário de greve no atual momento da universidade                        |
| Saúde         | Riscos relacionados à saúde e integridade física dos membros                                    |
| Fornecedores  | Riscos relacionados a prazo e qualidade de entrega dos fornecedores                             |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Análise Quantitativa e Qualitativa de Riscos

Na fase de Análise Quantitativa de Riscos, são realizadas avaliações numéricas da probabilidade, certeza e impacto dos riscos. Já na etapa de Análise Qualitativa de Risco, identificam-se quais riscos requerem maior atenção ou ações adicionais, considerando a relação entre a probabilidade de ocorrência e o impacto.[(1)](apendice8.md#referencias) As Tabelas 2, 3, 4 e 5 apresentam como esses critérios são determinados.


<figcaption style="text-align: center">
    <b>Tabela 2 – Identificação de peso da Probabilidade</b>
</figcaption>
<center>

| Probabilidade | Certeza     | Peso |
| ------------- | ----------- | ---- |
| Nulo          | 0%          | 0    |
| Muito baixa   | ]0%, 20%[   | 1    |
| Baixa         | [20%, 40%[  | 2    |
| Média         | [40%, 60%[  | 3    |
| Alta          | [60%, 80%[  | 4    |
| Muito alta    | [80%, 100%] | 5    |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Tabela 3 –Identificação de peso do Impacto</b>
</figcaption>
<center>

| Impacto     | Descrição                       | Peso |
| ----------- | ------------------------------- | ---- |
| Nulo        | Nulo                            | 0    |
| Muito Baixo | Pouco Expressivo                | 1    |
| Baixo       | Pouco Impacto                   | 2    |
| Médio       | Impacto Médio                   | 3    |
| Alto        | Grande Impacto                  | 4    |
| Muito Alto  | Impede a continuação do projeto | 5    |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Tabela 4 – Identificação de Grau de Risco</b>
</figcaption>
<center>

| Impacto X Probabilidade | Nulo | Muito Baixo | Baixo | Médio | Alto | Muito Alto |
| ----------------------- | ---- | ----------- | ----- | ----- | ---- | ---------- |
| Nulo                    | 0    | 0           | 0     | 0     | 0    | 0          |
| Muito Baixo             | 0    | 1           | 2     | 3     | 4    | 5          |
| Baixo                   | 0    | 2           | 4     | 6     | 8    | 10         |
| Médio                   | 0    | 3           | 6     | 9     | 12   | 15         |
| Alto                    | 0    | 4           | 8     | 12    | 16   | 20         |
| Muito Alto              | 0    | 5           | 10    | 15    | 20   | 25         |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Tabela 5 – Classificação do Grau de Risco</b>
</figcaption>
<center>

| Grau de Risco  | Descrição |
| -------------- | --------- |
| 0              | Nulo      |
| 0 < Risco <= 5 | Baixo     |
| 5 < Risco < 15 | Médio     |
| 15 <= Risco    | Elevado   |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Plano de Gerenciamento de Riscos

Os riscos presentes no projeto foram identificados pela equipe, assim como seus respectivos impactos, prevenções e mitigações. Tais aspectos são representados na Tabela 6.

<figcaption style="text-align: center">
    <b>Tabela 6 – Tabela do Plano de Gerenciamento de Riscos</b>
</figcaption>
<center>

| ID Risco | Risco                                                        | Consequência                                                                                         | Probabilidade | Impacto | Grau de Risco | Prevenção                                                         | Mitigação                                                                                 |
| -------- | ------------------------------------------------------------ | ---------------------------------------------------------------------------------------------------- | ------------- | ------- | ------------- | ----------------------------------------------------------------- | ----------------------------------------------------------------------------------------- |
| 1        | Trancamento/abandono da disciplina por integrantes do grupo  | O projeto pode deixar de ser entregue por completo                                                   | 2             | 4       | 8             | Boa comunicação, planejamento e organização ao longo do semestre  | Realocação das responsabilidades entre os membros remanescentes                           |
| 2        | Má definição do cronograma                                   | Menor tempo hábil para realização das tarefas                                                        | 2             | 4       | 8             | Atenção aos prazos da disciplina e tempo exigido para cada tarefa | Alocação de mais membros para a tarefa em atraso                                          |
| 3        | Dificuldade de comunicação entre os membros do grupo         | Dificuldade na realização das tarefas                                                                | 3             | 3       | 9             | Gestão ativa em comunicação com os membros                        | Tentar contatar o membro em questão por outros meios de comunicação                       |
| 4        | Má formulação do requisito                                   | Dificuldade de entendimento do requisito e realização incorreta do mesmo                             | 1             | 4       | 4             | Alinhamento da equipe acerca dos requisitos                       | Refatoração da atividade em questão                                                       |
| 5        | Escolha inadequada de tecnologias para realização do projeto | Incompatibilidade na integração dos componentes do projeto                                           | 2             | 4       | 8             | Estudo prévio acerca das tecnologias                              | Estabelecer um plano de contingência para rápida adaptação ou substituição de tecnologias |
| 6        | Qualidade do produto abaixo do esperado                      | Produto não atendendo totalmente à proposta inicial estabelecida                                     | 2             | 3       | 6             | Comunicação constante entre o diretor de qualidade e a equipe     | Refatoração dos elementos abaixo da qualidade esperada                                    |
| 7        | Insuficiência de recursos financeiros para o projeto         | Atraso ou interrupção das atividades devido à falta de verba para aquisição de materiais necessários | 3             | 5       | 15            | Estabelecimento de um orçamento detalhado e realista              | Reavaliação e priorização de despesas                                                     |
| 8        | Priorização inadequada das atividades a serem realizadas     | Dificuldade de entrega no prazo esperado                                                             | 3             | 4       | 12            | Revisão constante das prioridades                                 | Alocação de mais membros para realização de uma tarefa importante                         |
| 9        | Greve dos professores                                        | Mudança na data de entrega do projeto                                                                | 5             | 4       | 20            | -                                                                 | -                                                                                         |
| 10       | Atrasos ou defeitos nas entregas dos fornecedores            | Atraso na entrega do projeto                                                                         | 3             | 4       | 12            | Investigação prévia dos fornecedores                              | -                                                                                         |
| 11       | Problemas de saúde por parte dos membros                     | Sobrecarga dos membros restantes                                                                     | 2             | 4       | 8             | -                                                                 | Realocação de atividades entre os membros remanescentes                                   |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Referências

> 1.  PMI - PROJECT MANAGEMENT INSTITUTE. Guia PMBOK®: Um Guia para o Conjunto de Conhecimentos em Gerenciamento de Projetos, Sexta edição, Pennsylvania: PMI, 2017.

## Histórico de versões

| Versão | Alteração                                                         | Data       | Autor                                   |
| ------ | ----------------------------------------------------------------- | ---------- | --------------------------------------- |
| 1.0    | Criação do documento                                              | 27/04/2024 | Kayro César, Lucas Gabriel e Luan Vasco |
| 1.1    | Elaboração do EAR e da tabela do plano de gerenciamento de riscos | 28/04/2024 | Kayro César, Lucas Gabriel e Luan Vasco |
