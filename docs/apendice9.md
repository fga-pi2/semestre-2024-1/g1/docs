# Ferramentas utilizadas para o desenvolvimento do software

### Documentação e diagramação

#### Figma

Principal ferramenta do mercado para prototipação, várias features para desenvolvimento de protótipos de alta e baixa fidelidade,devtools e fluxos navegáveis.[(9)](projeto-software.md#referencias)

#### Draw.io

Ferramenta utilizada em várias áreas da Engenharia de Software, sendo algumas delas : MER, DER, Diagrama de classe, suporte a UML, diagrama de casos de uso, entre outras. A ferramenta foi utilizada para fazer diagramas. [(10)](projeto-software.md#referencias)

#### LucidChart

Ferramenta de diagramação de processos, notação UML e outros. A ferramenta foi utilizada para fazer diagramas. [(13)](projeto-software.md#referencias)

#### Whimsical

É uma ferramenta de mapas mentais e diagramas. Ela foi usada para a elaboração da Estrutura Analítica do Projeto (EAP), por causa da facilidade de criar diferentes níveis, por mover elementos entre níveis e pelo seu design elegante. [(12)](projeto-software.md#referencias)

#### MkDocs

O MkDocs é uma ferramenta que gera sites estáticos, facilitando a construção de documentação a partir de arquivos Markdown simples. O objetivo principal do MkDocs é simplificar a criação de documentação para desenvolvedores, escritores e criadores de conteúdo. [(11)](projeto-software.md#referencias)

### Versionamento de código

#### Git

O Git é um sistema de controle de versão distribuído utilizado para o gerenciamento de projetos de software. Criado por Linus Torvalds em 2005.A ferramenta propõe uma não linearidade, permitindo um mesmo projeto a ter milhares de ramificações, com desenvolvedores trabalhando em suas implementações independente dos demais.[(8)](projeto-software.md#referencias)

Suas vantagens são :

- Ramificações
- Pequeno e rápido
- Distribuído
- Gratuito e open-source
- Garantia dos dados (histórico de commits)

#### Gitlab

O GitLab é uma plataforma de gerenciamento e hospedagem de códigos, permitindo que os desenvolvedores trabalhem em projetos públicos e privados.
No caso do projeto de PI2, está sendo utilizada para hospedar a documentação e os códigos fontes do backend e do frontend.[(7)](projeto-software.md#referencias)

### Editor de texto

#### Visual Studio Code

Visual Studio Code, popularmente conhecido como Vscode, é um editor de texto adotado por muitos desenvolvedores para escreverem seus códigos. Possui recursos nativos como Debug, Git, Extensões, Terminal integrado e intellisense.
Está sendo utilizado na matéria para escrever os códigos e a documentação.[(6)](projeto-software.md#referencias)

## Linguagens de programação

### Python

Python é uma linguagem de programação de alto nível, criada por Guido van Rossum e lançada em 1991. Utilizada em diversas áreas de Software, como análise de dados, desenvolvimento web, aprendizado de máquina e automação. Sua sintaxe é julgada simples e possui uma variedade de bibliotecas e frameworks.[(2)](projeto-software.md#referencias)

### Javascript

JavaScript é uma linguagem de programação de alto nível, interpretada pelo navegador da web. Ela permite aos desenvolvedores adicionar comportamentos aos elementos HTML, manipular o conteúdo da página, responder a eventos do usuário e comunicar-se com servidores para atualizar dinamicamente o conteúdo da página sem recarregá-la. É a principal linguagem de programação adotada para o desenvolvimento web. E será utilizada no React Native.[(1)](projeto-software.md#referencias)

## Comunicação

### Discord

Discord é uma ferramenta de comunicação que possui servidores e canais baseados em tópicos, que permite a comunicação rápida e fluída por canais específicos. Tem suporte para voz e vídeo, bots, emojis e personalização.[(5)](projeto-software.md#referencias)
O grupo escolheu a ferramenta por :

- Facilidade ao criar servidores
- Facilidade ao trocar de canais
- Comunicação eficiente.

### WhatsApp

WhatsApp é um aplicativo de comunicação mobile, com extensão para a web, permite enviar e receber diversos tipos de ficheiros: texto, fotos, vídeos, documentos, bem como localização e chamadas de voz. Além do recurso de segurança que é a encriptação ponto a ponto.[(4)](projeto-software.md#referencias)

O grupo escolheu a ferramenta por :

- Todos já possuírem
- Facilidade ao criar grupos
- Comunicação eficiente.

## Armazenamento e compartilhamento de arquivos

### Google Drive

O Google Drive é um serviço de armazenamento em nuvem oferecido pelo Google, amplamente utilizada ao redor do globo. O usuário inicia com 15 GB gratuitos para uso. É fácil de compartilhar arquivos e pastas de forma segura. Além disso, o Google Drive oferece recursos de colaboração em tempo real, como edição simultânea de documentos, comentários e compartilhamento de links para visualização ou edição de arquivos. [(3)](projeto-software.md#referencias)

## Referências

<section id="referencias">
   
</section>

> 1. Javascript. Disponível em: https://developer.mozilla.org/pt-BR/docs/Web/JavaScript. Acesso em: 25 de abril de 2024.

> 2. Python. Disponível em: https://www.python.org/. Acesso em: 25 de abril de 2024.

> 3. Google Drive. Disponível em: https://www.google.com/intl/pt-br/drive/about.html. Acesso em: 25 de abril de 2024.

> 4. WhatsApp. Disponível em: https://www.whatsapp.com/about?lang=pt_pt. Acesso em: 25 de abril de 2024.

> 5. Discord. Disponível em: https://discord.com/. Acesso em: 25 de abril de 2024.

> 6. Visual Studio Code. Disponível em: https://code.visualstudio.com/. Acesso em: 25 de abril de 2024.

> 7. Gitlab. Disponível em: https://about.gitlab.com/platform/. Acesso em: 25 de abril de 2024.

> 8. Git. Disponível em: https://git-scm.com/. Acesso em: 25 de abril de 2024.

> 9. Figma. Disponível em: https://www.figma.com/. Acesso em: 25 de abril de 2024.

> 10. Draw.io. Disponível em: https://www.drawio.com/. Acesso em: 25 de abril de 2024.

> 11. HABBEMA, Hugo. MkDocs - Criar documentação para projetos Python. 28 out. 2023. Disponível em: https://medium.com/@habbema/mkdocs-c51c3a1c2bf5. Acesso em: 25 abr. 2024.

> 12. Whimsical - The iterative workspace for product teams. Disponível em: https://whimsical.com/. Acesso em: 22 maio. 2024.

> 13. LucidChart - Disponível em: https://www.lucidchart.com/pages/pt/produto. Acesso em: 30 de maio de 2024.

## Histórico de versões

| Versão | Alteração                           | Data       | Autor         |
| ------ | ----------------------------------- | ---------- | ------------- |
| 1.0    | Criação do documento com a versão 1 | 24/04/2024 | Luan Vasco    |
| 1.1    | Correções no documento              | 28/04/2024 | Kayro César   |
| 1.2    | Ajustes estruturais no documento    | 30/05/2024 | Lucas Gabriel |