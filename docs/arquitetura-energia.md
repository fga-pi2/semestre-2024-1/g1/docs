# Arquitetura de Energia

Para a alimentação do projeto Ovnitrap, duas baterias em paralelo (totalizando 24V) serão carregadas por meio de uma placa solar que terá sua tensão controlada por um controlador de carga PWM. O sistema inteiro opera em corrente contínua, sendo a bomba e a válvula de tensões nominais de 24V e os sensores ligados a um regulador de 5V, por operarem com uma diferença de potencial menor, conforme diagrama abaixo. Além disso, a potência do sistema vai ser medido por meio de um módulo de sensor de corrente e tensão.

<figcaption style="text-align: center">
    <b>Figura 1: Diagrama de energia</b>
</figcaption>

<center>

![Diagrama de energia](assets/images/Arquiteturaenergia3.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Histórico de versões

| Versão | Alteração                              | Data       | Autor                 |
| ------ | -------------------------------------- | ---------- | --------------------- |
| 1.0    | Documentação da arquitetura de energia | 26/04/2024 | Miguel Meira Wichoski |
| 2.0    | Correção da arquitetura de energia | 02/04/2024 | Miguel Meira Wichoski |
| 3.0    | Melhoria do diagrama| 02/04/2024 | Miguel Meira Wichoski |
