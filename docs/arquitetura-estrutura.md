# Arquitetura de Estrutura

A estrutura do Ovnitrap é composta por plástico, madeira, tubos PVC, perfis de alumínio e metalon. O uso desses materiais foi escolhido devido à sua resistência à corrosão causada pela água, sua leveza, fácil manuseio e seu custo acessível. A principal parte da estrutura é um balde de formato retangular de 20 litros, projetado para conter a água utilizada no Ovnitrap. A tampa do balde será apoiada por uma madeira para acomodar um bocal de cano PVC, dando maior resistência. Nesse bocal, será feita um furo no cap, onde passará mangueira maleável que funcionará com o princípio do Copo de Pitágoras, garantindo a eficácia do dispositivo na captura das lavas de mosquitos. Na parte superior será fixada a mangueira que levará água armazenada no balde, LEDs e câmera.

Além disso, será desenvolvida uma estrutura em metalon para fixação da placa solar e baterias que são necessários para o funcionamento do Ovnitrap. Essa estrutura garantirá a integridade dos dispositivos eletrônicos, proporcionando uma operação confiável do sistema.
O projeto estrutural também busca uma estética visualmente agradável, combinando com vários tipos de ambiente e sendo atrativo para os mosquitos. A utilização de materiais plásticos e alumínio permitem uma boa modelagem para esse tipo de design, ou seja, locais com árvores e plantas e cantos de quintais de casas.

## Diagrama de arquitetura de estrutura

<figcaption style="text-align: center">
    <b>Figura 1: Diagrama de arquitetura de estrutura</b>
</figcaption>

<center>

![Diagrama de arquitetura de estrutura](assets/images/arquitetura-estrutura.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Histórico de versões

| Versão | Alteração            | Data       | Autor      |
| ------ | -------------------- | ---------- | ---------- |
| 1.0    | Criação do documento | 25/04/2024 | Carlos Vaz |
| 1.1    | Correções gerais     | 31/05/2024 | Carlos Vaz |
| 1.2    | Correções gerais     | 11/07/2024 | Carlos Vaz |