# Arquitetura de Software

Arquitetura de software é a estrutura fundamental ou o esqueleto de um sistema de software, que define seus componentes, suas relações e seus princípios de projeto e evolução [(1)](arquitetura-software.md#referencias).

## Diagrama de Arquitetura de Software

O diagrama de arquitetura de software é um diagrama que ilustra visualmente a organização de um sistema ou aplicativo de software. Essa representação é fundamental para comunicar como os diversos componentes do sistema se relacionam e colaboram para alcançar os objetivos do projeto[(2)](arquitetura-software.md#referencias).

Tendo em vista o entendimento do problema e os requisitos levantados para o produto proposto, o diagrama apresentado na Figura 1 representa a arquitetura de software planejada, enquanto na Figura 2 se tem a arquitetura utilizada no projeto.

<figcaption style="text-align: center">
    <b>Figura 1: Diagrama de arquitetura - versão 1.0 </b>
</figcaption>

<center>

![Diagrama de arquitetura - versão 1.0](assets/images/diagrama-arquitetura.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>


<figcaption style="text-align: center">
    <b>Figura 2: Diagrama de arquitetura - versão 2.0 </b>
</figcaption>

<center>

![Diagrama de arquitetura - versão 2.0](assets/images/diagrama-arquitetura2.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Protótipo de Alta Fidelidade do Aplicativo Mobile

### Descrição

Este protótipo foi desenvolvido no Figma para ilustrar o aplicativo mobile que estamos desenvolvendo para monitorar nosso projeto de armadilha de mosquito da dengue. O aplicativo visa fornecer uma interface intuitiva e eficaz para os usuários acompanharem e gerenciarem o projeto de monitoramento de mosquitos da dengue.

O protótipo inclui as seguintes telas principais:

#### 1. Acessar Dispositivo

Esta tela permite que os usuários acessem os dispositivos de monitoramento de mosquito da dengue. Eles podem conectar-se aos dispositivos por meio de Bluetooth ou Wi-Fi e configurar as opções de monitoramento.

#### 2. Monitoramento

A tela de Monitoramento exibe informações sobre a atividade de larvas capturadas pelos dispositivos. Os usuários podem visualizar dados como a contagem de larvas, a temperatura ambiente e outros.

#### 3. Dashboards

O Dashboard fornece uma visão geral das informações da armadilha. Ele exibe gráficos e estatísticas relacionadas à atividade das larvas ao longo do tempo, permitindo que os usuários identifiquem tendências e tomem medidas apropriadas.

#### 4. Ajuda

A tela de Ajuda oferece suporte aos usuários, fornecendo informações sobre como usar o aplicativo, solucionar problemas comuns e entrar em contato com a equipe de suporte técnico.

#### 5. Imagens

A tela de Imagens lista as fotos captadas pela câmera interna da armadilha, para o usuário poder visualizar a imagem referente à atividade registrada.

#### Barra de navegação

Para auxiliar a navegação entre as telas disponíveis, foi criada uma barra no canto inferior da tela.

### Conclusão

Este protótipo do Figma serve como uma representação visual do aplicativo mobile e será utilizado para validar e iterar sobre o design antes do desenvolvimento final. A seguir podem ser observados as duas versões de protótipos produzidas para a aplicação.

<figcaption style="text-align: center">
    <b>Protótipo de média fidelidade</b>
</figcaption>
<center>
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Ffile%2FKYXHbF3q7JsM97aqxoKebZ%2FUntitled%3Ftype%3Ddesign%26node-id%3D0%253A1%26mode%3Ddesign%26t%3DMxjf1hd9FxMHHRTc-1" allowfullscreen></iframe>
</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

<figcaption style="text-align: center">
    <b>Protótipo de alta fidelidade</b>
</figcaption>
<center>
<iframe style="border: 1px solid rgba(0, 0, 0, 0.1);" width="800" height="450" src="https://www.figma.com/embed?embed_host=share&url=https%3A%2F%2Fwww.figma.com%2Fdesign%2FKjXvC7IjhZ3JbaQAvLveOX%2FProt%25C3%25B3tipo-de-Alta-Fidelidade%3Fnode-id%3D0-1%26t%3Druz0aKiPFq4d0swV-1" allowfullscreen></iframe>
</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Referências

> 1. ISO/IEC/IEEE. ISO/IEC/IEEE 42010:2011 - Systems and software engineering - Architecture description. Geneva: ISO, 2011.

> 2. AMAZON. O que é a diagramação de arquitetura? 2023. Acesso em: 29 set. 2023. Disponível em: https://aws.amazon.com/pt/what-is/architecture-diagramming/.

## Histórico de versões

| Versão | Alteração                                                          | Data       | Autor                                   |
| ------ | ------------------------------------------------------------------ | ---------- | --------------------------------------- |
| 1.0    | Criação do documento                                               | 26/04/2024 | Kayro César, Luan Vasco e Lucas Gabriel |
| 1.1    | Documentação do protótipo                                          | 26/04/2024 | Victor Yukio                            |
| 1.2    | Documentação do protótipo de alta fidelidade e da nova arquitetura | 12/07/2024 | Lucas Gabriel                           |
