import RPi.GPIO as GPIO
import time
from datetime import datetime

# Configuração do pino GPIO
RELE_PIN = 22  # pino GPIO que está conectado ao relé

# Configuração do GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(RELE_PIN, GPIO.OUT)

def acionar_rele():
    # Liga o relé
    GPIO.output(RELE_PIN, GPIO.LOW)  # Assumindo que LOW liga o relé; 
    print("Bomba ligada")
    time.sleep(30)  # Mantém a bomba ligada por 30 segundos
    # Desliga o relé
    GPIO.output(RELE_PIN, GPIO.HIGH)
    print("Bomba desligada")

# Variável de controle para saber se a bomba já foi ligada
bomba_ligada = False

try:
    while True:
        # Obtém a hora atual
        agora = datetime.now()
        hora_atual = agora.hour
        minuto_atual = agora.minute

        # Verifica se está dentro do horário das 17:00 às 17:59 e se a bomba ainda não foi ligada
        if hora_atual == 17 and not bomba_ligada:
            acionar_rele()
            bomba_ligada = True  # Atualiza a variável para indicar que a bomba já foi ligada
        
        # Reseta a variável de controle após o intervalo de 17:59
        if hora_atual == 18:
            bomba_ligada = False
        
        # Espera 60 segundos antes de verificar novamente
        time.sleep(60)

except KeyboardInterrupt:
    # Limpa a configuração do GPIO
    GPIO.cleanup()
    print("Programa encerrado")

finally:
    # Limpa a configuração do GPIO no final do programa
    GPIO.cleanup()
