# Execução de Eletrônica

## Aquisição dos Componentes

Os seguintes componente foram adquiridos:

- Raspberry pi 3 +B
- Webcam C925e
- Relé JQC3F Temporizador Digital Ajustável
- Bomba de água 22w 800l/h 24v Dc
- Sensor de temperatura Ds18b20
- Sensor De Nível De Água Interruptor De Boia
- LED para iluminação.
- Módulo GPS.

Os seguintes componente foram substituidos ou descartados

- Sensor de distancia ultrassom
- Válvula Solenoide 2/2 Vias N. Fechada 3/4

## Construção

A implementação do projeto foi iniciada com a compra dos componentes no sitio especializado em vendas Mercado Livre [(1)](execucao-eletronica.md#referencias) e lojas fisicas. Terminada a etapa de compras, de acordo com que os componentes eram recebidos, foram testados de forma individual: a bomba de agua, os sensores de nível e de temperatura e os réles utilizando apenas códigos simples para testes sem integração.

Em um dos réles foi identificado que estava danificado, mas não foi substituido pois foram comprados dois conjuntos de réles, a fim de previnir falhas já esperadas como essa.

A etapa seguinte foi a construção de cabos para conectar os componentes. Essa parte foi complexa pois muitos componentes exigiam diferentes tipo de cabos.

- **Raspberry pi 3 +B**: A Raspberry foi ligada aos componentes utilizando "Jumpers", que são os fios para conexãos dos GPIOs. Os Jumpers foram parafusados aos réles e soldados às extensões dos sensores de nivel e temperatura. Essa forma de conexão não seria utilizada em um produto comercial, a forma correta seria produzindo uma placa de circuito impresso com os conectores corretos soldados, produzindo conexões mais robustas e não propensas ao fácil rompimento. Porém apenas para testes essa solução é adequada.

- **Webcam C925e**: Foi connectada a Raspberry utilizando um cabo usb. A biblioteca utilizada "opencv" já possui todos comandos para utilizar a câmera sem maiores dificuldades.

- **Relé JQC3F Temporizador Digital Ajustável**: Sua alimentação foi realizada ligando diretamente à bateria de 24V para operação. A fonte de 5V foi ligada à porta acionada pelo réle, dessa forma, o réle controla quando a Raspberry liga e desliga. A forma de funcionamento será ligando a cada 1 (uma) hora durante 10 minutos. Essa configuração foi realizada no próprio sistema nativo do componente que não precisa ser ligado de nenhuma forma logica ao restante do projeto.

- **VK-172**: O componente escolhido, que possui GPS e conexão de internet 3G que foi apresentado no ponto de controle 1 foi descartado em função do componente comprado apresentar problemas de hardware. Foi substiutuído pelo módulo VK-172 que possui apenas GPS e a internet será fornecida por WiFi local.

<center>

<figcaption style="text-align: center">
    <b>Figura 1 - Testando réles</b>
</figcaption>

![Testando réles](assets/images/testando-reles.jpeg)

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>
</center>

- **Bomba de água 22w 800l/h 24v Dc**: Esse componente foi ligado a saida de um dos réles controlados pela Raspberry. Foi ligado utilizando um connector XT-60 para facilitar a conexão durante o transporte e montagem. A forma de funcionamento final ainda será melhor definida, mas o controle do componente será realizado com um script e comandos ao réle ligado a ele.

- **Sensor de temperatura Ds18b20**: Os fios para conexão eram curtos e foram ligados a extensões para facilitar a conexão. Será colado na parede do reservatório superior utilizando adesivo termoplástico.

- **Sensor De Nível De Água - Interruptor De Boia**: Foi implementado utilzando uma peça de plástico como ancora e colado ao fundo do reservatório inferior para avisar caso o nivel desça além desse ponto.

- **LED para iluminação**: Foram soldados em uma placa perfurada vários LEDs e ela será fixada utilizando adesivo termoplástico ao reservatório de água superior.

<center>

<figcaption style="text-align: center">
    <b>Figura 2 - Led Instalado</b>
</figcaption>

![Leds](assets/images/leds.jpeg)

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>
</center>

## Resultados

Os componentes então foram testados em conjunto no dia 31/05/2024. A Raspberry realizou os comandos do réle da bomba corretamente. A captura dos dados e fotos também foram realizadas com sucesso. O módulo GPS conseguiu ligar e encontrar satelites, porém não foi possivel obter a posição, provavelmente em função de um erro no código utilizado, o qual será corrigido até o ponto de controle 3.

## Referências

> 1. Mercado Livre Brasil - Frete Grátis no mesmo dia. Disponível em: <https://www.mercadolivre.com.br>. Acesso em: 2 jun. 2024.

## Histórico de Versão

| Versão | Alteração            | Data       | Autor               |
| ------ | -------------------- | ---------- | ------------------- |
| 1.0    | Criação do documento | 02/06/2024 | Gabriel e Luís Lins |
