# Execução de Energia

## Aquisição de Componentes

Os seguintes componente foram adquiridos:

- Controlador de carga PWM;
- Duas baterias 12V, 7Ah;
- Uma placa fotovoltaica de 2,5 metros quadrados, com 45 Watts de potência;
- Multímetro INA3221.

## Construção

A implementação do projeto foi iniciada com a compra dos componentes no site de vendas Mercado Livre. Terminada a etapa de compras, de acordo com que os componentes eram recebidos, foram testados de forma individual, inicialmente com o controlador de carga. Inicialmente, foi testado usando uma fonte do laboratório de eletrônica da FGA, fazendo testes de cada modo de operação desejado do equipamento. Em seguida, conforme solicitado, foram concedidos uma placa fotovoltaica de pequeno porte para a geração de energia do projeto, com isso, foram feitos testes de funcionamento da placa utilizando um voltímetro. Além da placa, foram solicitadas ao professor Alex as duas baterias mencionadas, visto que o fornecedor escolhido atrasou a entrega das baterias.

<center>

<figcaption style="text-align: center">
    <b>Figura 1 - Controlador e Placa Fotovoltaica</b>
</figcaption>

![Controlador e Placa Fotovoltaica](assets/images/controlador-energia.jpeg)

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>
</center>

Com o funcionamento de cada componente de forma isolada, foram conectados todos os componentes no controlador de carga, funcionando de forma adequada.

A obtenção dos dados do nível da bateria foi realizada através do [código](https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/docs/-/blob/main/docs/assets/codigos/SDL_Arduino_INA3221_Test.py?ref_type=heads) que funciona conforme o descrito abaixo:

- É feita a configuração do GPIO da raspberry;
- É aferido a amperagem de saída, voltagem e potência da bateria;
- Com isso, é enviado para o servidor, assim que tiver conexão com a internet.

## Resultados

Os componentes então foram testados em conjunto no dia 31/05/2024. O controlador de carga funcionou como o esperado, com exceção da entrega de energia na Raspberry durante seu pico de energia, com isso, será necessário utilizar a entrada USB deste controlador. Os outros componentes funcionaram como o esperado.

## Histórico de Versão

| Versão | Alteração                  | Data       | Autor               |
| ------ | -------------------------- | ---------- | ------------------- |
| 1.0    | Criação do documento       | 02/06/2024 | Miguel              |
| 1.1    | Revisão e adição de imagem | 02/06/2024 | Gabriel e Luís Lins |
