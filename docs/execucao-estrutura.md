# Execução de Estrutura

## Aquisição de Material

Os seguintes componente foram adquiridos:

- Canos PVC de 100mm de variados tamanhos
- Reservatório de água em formato de caixa
- Tinta preta
- Prancha de madeira
- Mangueiras 1/2'' e 1/4'' de 1 metro
- Perfil de alumínio
- Metalon
- Parafusos, porcas e braçadeiras

## Construção

No dia 24/05/2024 as peças foram levadas ao prédio LDTEA. Junto ao professor Rhander, foram furadas e cortadas nas dimensões previamente definidas. As peças que compoem o reservatório superior foram pintadas de preto pois na literatura é sempre destacado que os mosquitos tem mais afinidade com ambiente escuros. A estrutura de forma geral então foi montada utilizando adesivo termoplastico e adesivo epoxy. 

No dia 28/05/2024 foi finalizada a fabricação e montagem do carrinho de metalon, na qual foi necessário fazer os cortes, lixamento, solda e fixação das rodinhas.

<center>

<figcaption style="text-align: center">
    <p>Figura 1 - Pintando estrutura de preto</p>
</figcaption>

![Pintando de Preto](assets/images/pintando.jpeg)

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>
</center>

Foi utilizado uma tábua de madeira para ajudar a equilibrar a parte do reservatório superior. Foram feitos os furos e instalados os tubos de acionamento do descarte da água, os quais foram testados e aprovados.

<center>

<figcaption style="text-align: center">
    <p>Figura 2 - Estrutura montada</p>
</figcaption>

![Estrutura Montada](assets/images/estrutura.jpeg)

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>
</center>

A estrutura para suporte da placa solar foi cortada e montada junto com o professor Rhander, utilizando peças em aluminio.

<center>

<figcaption style="text-align: center">
    <p>Figura 3 - Carrinho de metalon não finalizado</p>
</figcaption>

![Carrinho de metalon não finalizado](assets/images/carrinho-incompleto.jpg)

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>
</center>

O carrinho foi soldado por conta própria e levado a faculdade posteriormente para finalização. Na faculdade foi feito o trabalho de lixamento, pintura de preto, acabamento e adição das rodinhas.

<center>

<figcaption style="text-align: center">
    <p>Figura 4 - Estrutura sendo finalizada</p>
</figcaption>

![Estrutura sendo finalizada](assets/images/finalizacao-estrutura.jpg)

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>
</center>

## Resultados

A estrutura funcionou corretamente, a água conseguiu ser descartada de forma automática. O equilibrio da estrutura não apresentou problemas, visto que foi realizado com o peso da água no reservatório inferior assim como uma caixa de forma retangular e larga. O carrinho foi concluído sem muitos problemas, e suportou com facilidade todos os pesos e fixações na qual foi submetido.

## Histórico de Versão

| Versão | Alteração            | Data       | Autor               |
| ------ | -------------------- | ---------- | ------------------- |
| 1.0    | Criação do documento | 02/06/2024 | Gabriel e Luís Lins |
| 1.1    | Adições gerais       | 11/07/2024 | Carlos Vaz          |