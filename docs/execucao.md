Nesta seção estão os detalhes de execução de cada subsistema envolvido no projeto, tal como os resultados alcançados até o momento.

Abaixo estão os links para os detalhes da execução de cada um dos subsistemas do projeto:

- [Execução de Eletrônica](./execucao-eletronica.md)
- [Execução de Energia](./execucao-energia.md)
- [Execução de Software](./execucao-software.md)
- [Execução de Estrutura](./execucao-estrutura.md)

## Resultados alcançados até o Ponto de Controle 2

- **Detalhamento do Problema**: Detectamos que a dengue teve um impacto grande em 2024 e são necessárias mais informações para um combate devido.
- **Identificação de uma solução**: Após analisar as soluções do mercado, definimos que um bom produto seria obter dados georeferenciados detalhados da ploriferação do mosquito vetor da dengue.
- **Projeto da solução e de seus subsistemas**: Cada componente do sistema foi pensado e projetado para desenvolver o produto concebido, isso inclui cálculos e diagramas.
- **Construção do projeto e protótipo do produto**: Elaboração de cada um dos subsistemas individualmente.

<center>

<figcaption style="text-align: center">
    <b>Figura 1 - Estrutura Completa</b>
</figcaption>

![Leds](assets/images/teste%20geral.jpeg)

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>
</center>

## Histórico de Versão

| Versão | Alteração            | Data       | Autor               |
| ------ | -------------------- | ---------- | ------------------- |
| 1.0    | Criação do documento | 02/06/2024 | Gabriel e Luís Lins |
