**Universidade de Brasília - UnB**

**Faculdade UnB Gama - FGA**

**Brasília, DF. 2024**

# Ovnitrap

Bem-vindo(a) à documentação do **Ovnitrap**! Aqui você conhecerá como ocorreu a execução do projeto, a documentação gerada e o produto produzido, tal como suas principais funcionalidades.

## Sobre o Projeto

O Ovnitrap é um projeto integrador de 5 engenharias (eletrônica, software, energia, automotiva e aeroespacial) que visa combater o avanço do mosquito da dengue. Estamos focados em:

- Identificar locais com foco do mosquito;
- Impedir que larvas se tornem novos mosquitos;
- Auxiliar autoridades e organizações no monitoramento do _Aedes aegypti_;
- Permitir estudos com as larvas capturadas.

## Principais Recursos

- **Feature 1:** Armadilha que captura larvas do mosquito;
- **Feature 2:** Aplicativo que monitora o estado da armadilha e de atividade das larvas;
- **Feature 3:** Geração de relatório com dados de atividade e geolocalização.

## Equipe

<figcaption style="text-align: center">
    <b>Tabela 1: Informações sobre a equipe do projeto</b>
</figcaption>
<center>

| Integrante                         | Matrícula |          Curso          |         Papel          |
| :--------------------------------- | :-------: | :---------------------: | :--------------------: |
| Gabriel Bacon Carvalho             | 180136569 |  Engenharia Eletrônica  |   Coordenador Geral    |
| Luís Guilherme Gaboardi Lins       | 180022962 | Engenharia de Software  |  Diretor de Qualidade  |
| Luan Vasco Cavalcante              | 190111836 | Engenharia de Software  |  Diretor de Software   |
| Jhéssica Isabel Coelho Souza       | 170146031 |  Engenharia Eletrônica  | Diretora de Eletrônica |
| Miguel Meira Wichoski              | 200064096 |  Engenharia de Energia  |   Diretor de Energia   |
| Carlos Eduardo Vaz Ferreira        | 190139439 | Engenharia Aeroespacial |  Diretor de Estrutura  |
| Luiz Guilherme Palhares Pettengill | 150138202 | Engenharia de Software  |     Desenvolvedor      |
| Kayro César Silva Machado          | 170107426 | Engenharia de Software  |     Desenvolvedor      |
| Lucas Gabriel Sousa Camargo Paiva  | 190112123 | Engenharia de Software  |     Desenvolvedor      |
| Victor Yukio Cavalcanti Miki       | 180068229 | Engenharia de Software  |     Desenvolvedor      |

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>


© 2024 Ovnitrap. Feito com ❤️
