O projeto de eletrônica lista e explica o uso de componentes relacionados ao sistema de telemetria e controle da solução.

## **Raspberry pi 3 +B**

<figcaption style="text-align: center">
    <b>Figura 1: Raspberry</b>
</figcaption>

<center>

![Raspberry](assets/images/raspberry-pi-3b.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: <a href="https://www.raspberrypi.com/products/raspberry-pi-3-model-b-plus/"> Raspberry Pi 3 Model B+ - The final revision of our third-generation single-board computer </a></small>
</figcaption>

A Raspberry foi escolhida porque é um computador de placa única, significa que implementa todos os componentes de forma pequena e eficiente apesar de ter a capacidade de funcionalidades de um computador completo com processador, memoria ram e placa de video. Dessa forma é capaz de processar informações e utilizar um sistema operacional embarcado comum Linux de fácil implementação que facilita a realização do projeto.

O tamanho e o fato de ser um computador feito e projeto por uma única empresa também significa que possui uma integração bem otimizada e nesse caso um consumo energético pequeno. O projeto pode utilizar isso pois visa um dispositivo que possa funcionar sem intervenção humana por muitos dias. O baixo consumo então permite o uso racional da energia fornecida pela bateria.

O computador Raspberry também permite conexão WiFi que é essencial para o projeto além da implementação de um modem GSM para ambientes sem internet local. O numero de GPIOs também foi importante para a escolha da Raspberry que possui 40 pinos GPIO e como os dispositivo utiliza muitos sensores e atuadores, esse número atendeu a todas as demandas e ainda tem pinos sobrando para mudanças no futuro do projeto.

<figcaption style="text-align: center">
    <b>Tabela 1: Especificações Raspberry pi 3b+</b>
</figcaption>

<center>

| Parâmetros                             | Especificações                                                        |
| -------------------------------------- | --------------------------------------------------------------------- |
| Tensão de entrada                      | 5 V (entrada)                                                         |
| Processador                            | Broadcom BCM2837B0 quad-core Cortex-A53 de 64 bits                    |
| Velocidade do Processador              | 1,4 GHz                                                               |
| Memória RAM                            | 1 GB LPDDR2                                                           |
| GPIOs                                  | 40 pinos                                                              |
| Entradas Conversoras Analógica/Digital | A Raspberry Pi 3 B+ não possui conversores analógico/digital internos |
| UART                                   | 1 UART             

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## **Webcam C925e**

<figcaption style="text-align: center">
    <b>Figura 2: Webcam C925e</b>
</figcaption>

<center>

![Webcam C925e](assets/images/webcam-C925e.png)

</center>

<figcaption style="text-align: center">
     <small>Fonte: <a href="https://www.logitech.com/pt-br/products/webcams/c925e-business-webcam.960-001075.html"> Logitech - Webcam para negócios aprimorada de 1080p com suporte a H.264 </a></small>
</figcaption>

Essa webcam foi escolhida pois o grupo já a possuía. O fato de utilizar uma resolução 1080p é suficiente e perfeito para implementar o reconhecimento de imagens e testar suas capacidades para esse cenário. O reconhecimento de número de larvas pela camera não deve ser tão difícil quanto outras situações como reconhecimento de placas de carros ou facial, isso porque a imagem será sempre a mesma e com a luz controlada. A imagem será checada para questão de luminosidade e caso necessário será desligado ou ligado os LED.
Para a etapa 2 do projeto foi implementado um <a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/docs/-/blob/main/docs/assets/codigos/capture_and_upload.py?ref_type=heads">código</a> que funciona da seguinte forma: 
- Aguarda 3 segundos para estabilização da câmera;
- Faz a captura de 3 fotos em sequência com intervalo de 1 segundo entre cada captura;
- Armazena as fotos num diretório;
- Envia para o servidor assim que a conexão com a internet estiver disponível.

<b>*Compatibilidade e Integração Técnica:*</b>

A integração da Webcam C925e foi relativamente direta, pois a Raspberry Pi suporta dispositivos USB nativamente. O desafio principal foi a estabilização e a sincronização da captura de imagens. A estabilização da câmera antes de capturar imagens e o envio das fotos para o servidor foram processos bem geridos com scripts que controlam os tempos e a qualidade das imagens capturadas.

## **Relé JQC3F Temporizador Digital Ajustável**

<figcaption style="text-align: center">
    <b>Figura 3: Relé JQC3F Temporizador Digital Ajustável</b>
</figcaption>

<center>

![Relé JQC3F Temporizador Digital Ajustável](assets/images/reles.png)

</center>

<figcaption style="text-align: center">
     <small>Fonte: <a href="https://www.casadarobotica.com/sensores-modulos/modulos/timers/rele-temporizador-digital-ajustavel-delay-timer-para-chocadeira-e-usos-em-geral"> Casa da Robótica - Relé Temporizador Digital Ajustável Delay Timer </a></small>
</figcaption>

Esse dispositivo temporizador e modelo de relé foi escolhido pois possui um sistema integrado que permite a temporização individual sem estar sob o controle da Raspberry, isso é necessário para ligar e desligar a Raspberry que não possui essa função nativa, apenas ligando no momento que é fornecido tensão na entrada de 3V da mesma. Dessa forma será evitado que a Raspberry fique ligada no momento que não tem operações sendo realizadas e menor custo energético para o projeto. Esse relé pode ser alimentado diretamente pela bateria de 24v e isso facilita a implementação. Será utilizado outros dois relés do mesmo modelo mas sem sistema temporizador pois serão usados para operar a bomba e LEDs. Esses serão controlados pela Raspberry quando estiver ligada.

<br>
<b>Características de Corrente, Tempo de Resposta e Durabilidade:</b>

*Capacidade de Corrente:* Este relé possui uma capacidade nominal de corrente de até 10A, o que é suficiente para a maioria dos dispositivos que precisam ser controlados no projeto, como a Raspberry Pi. <br>
*Tempo de Resposta:* O tempo de resposta do relé JQC3F é aproximadamente 10 ms (milissegundos), o que é adequado para as necessidades do projeto, garantindo que os dispositivos sejam ligados e desligados rapidamente.<br>
*Durabilidade:* O JQC3F é projetado para uma longa vida útil, com uma durabilidade de cerca de 100.000 operações elétricas. Essa durabilidade é importante para garantir a confiabilidade do sistema ao longo do tempo.

<b>*Compatibilidade e Integração Técnica:*</b>

A utilização de um relé temporizador fora do controle direto da Raspberry Pi foi uma escolha estratégica para reduzir o consumo energético, já que permitirá ligar a raspberry apenas em horários de operação, evitando que a mesma fique ligada e consumindo energia o tempo inteiro. Isso é nescessario pois a raspberry nao possui um sistema nativo de baixo consumo, logo ela quando ligada consome constantemente 3 watts e no max 15 watts em momento de pico. Dessa forma o relé garante que até mesmo esses 3 watts pode ser evitado o consumo que provavelmente será bem menor pelo relé, apesar que não foi possivel confirmar o consumo do relé durante os trabalhos. O desafio aqui foi sincronizar a temporização do relé com as operações da Raspberry Pi, garantindo que a Pi estivesse desligada quando não fosse necessária, sem perder dados críticos ou comprometer o funcionamento do sistema.


## **Módulo Rele 2 canais com optoacoplador**

<figcaption style="text-align: center">
    <b>Figura 4: Módulo Rele 2 canais com optoacoplador</b>
</figcaption>

<center>

![Módulo Rele 2 canais com optoacoplador](assets/images/rele.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: <a href="https://www.eletrogate.com/modulo-rele-2-canais-3v-10a-com-borne-kre-para-esp32?utm_source=Site&utm_medium=GoogleMerchant&utm_campaign=GoogleMerchant&utm_source=google&utm_medium=cpc&utm_campaign=[MC4]_[G]_[PMax]_ArduinoRoboticaSensoresModuloss&utm_content=&utm_term=&gad_source=4&gclid=Cj0KCQjwhb60BhClARIsABGGtw9PvG0o-KZsLGvJn_n5PreJVj48mGGiIVEb8Yfzjl-r1ZnO69Mtn3oaApXcEALw_wcB"> Eletrogate- Módulo Relé 2 Canais 3V 10A com Borne KRE </a></small>
</figcaption>

Esse Módulo Relé possui 2 reles de 1 canal 3V com interface padrão TTL, que pode ser controlado diretamente por diversos Microcontroladores (Arduino, 8051, AVR, PIC, DSP, ARM, ARM, MSP430, Raspberry).

Na etapa 2 do projeto foram implementados códigos de acionamento dos relés para o controle dos leds e da bomba de água.
O <a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/docs/-/blob/main/docs/assets/codigos/led_control.py?ref_type=heads">código</a> de acionamento do relé que controla os Leds funciona da seguinte forma:
- É definido o pino GPIO da raspberry que está conectado ao relé;
- O GPIO é configurado como saída;
- O relé recebe nível lógico alto acionando os leds durante 15 segundos, tempo suficiente para a captura das fotos;
- Após os 15 segundos o relé é desativado, desligando os leds.

<b>Características de Corrente, Tempo de Resposta e Durabilidade:</b>

*Capacidade de Corrente:* Cada canal do módulo relé pode suportar correntes de até 10A a 250V AC ou 30V DC, o que o torna ideal para controlar dispositivos de alta corrente, como a bomba de água. <br>
*Tempo de Resposta:* Os relés no módulo possuem um tempo de resposta rápido, na faixa de 5 a 10 ms, garantindo operações rápidas e eficientes.<br>
*Durabilidade:* Com uma durabilidade de cerca de 100.000 operações elétricas, os relés no módulo são adequados para uso prolongado no projeto, proporcionando confiabilidade a longo prazo.

<b>*Compatibilidade e Integração Técnica:*</b>

A compatibilidade do módulo relé com a Raspberry Pi foi facilitada pela interface padrão TTL. O uso de optoacopladores 817C ajudou a proteger a Raspberry Pi de picos de corrente e interferências, um desafio comum em projetos que envolvem acionamento de dispositivos de alta corrente, como bomba de água, aumentando a segurança e a integridade do sistema.

Garantir que os relés selecionados fossem adequados para as correntes e tensões dos dispositivos conectados foi um desafio. Isso porque a maioria dos relés encontrados no mercado são operados com a tensão de 5V, pois é a tensão utilizada por grande parte das placas de desenvolvimento como o arduino. Nesse caso então foi escolhido um que dê originalmente operasse com os 3,3V fornecidos pela raspberry e foi a forma encontrada de lidar com esse problema. A outra forma estudada seria utilizar um controlador arduino comandado pela raspberry para operar os relés, mas isso seria um trabalho a mais desnecessário.



## **Bomba de água 22w 800l/h 24v Dc**

<figcaption style="text-align: center">
    <b>Figura 5: Bomba de água 22w 800l/h 24v Dc</b>
</figcaption>

<center>

![Bomba de água 22w 800l/h 24v Dc](assets/images/bomba.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: <a href="https://www.magazineluiza.com.br/mini-bomba-d-agua-24v-22w-800l-h-entrada-saida-1-2-pol-d7-eng-automacao/p/cdb30kfhdg/cj/mobo/?seller_id=oliststore&srsltid=AfmBOorE6MMBdOB69sBm9csM3-DOyXkXa9dgwq8JsnF7nZuJevYssZuNqnU"> Magazine Luiza - Mini Bomba D'água 24V 22W 800L/h Entrada/Saída 1/2 Pol. (D7) - ENG Automação </a></small>
</figcaption>

Essa bomba foi escolhida por apresentar a tensão de operação correta para o projeto. O fluxo de água que é possível movimentar também é importante pois é necessário um fluxo alto para garantir que nenhuma larva fique no reservatório superior e seja descartada. Uma bomba de aquário pequena de 12V consegue realizar um fluxo de 200L/h enquanto essa bomba consegue 4x com um valor de 800L/h e ainda está dentro do que é possível ser operado pelo conjunto de baterias. O consumo de energia é importante mas o período de utilização será pequeno de apenas alguns segundo deixando o consumo baixo.

O acionamento da bomba é feito através de um relé que é controlado através da raspberry, com um <a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/docs/-/blob/main/docs/assets/codigos/bomb_control.py?ref_type=heads">código</a> que funciona conforme o descrito abaixo: 
- É feita a configuração do GPIO da raspberry;
- Verifica a hora atual;
-  Verifica se está dentro do horário das 17:00 às 17:59 e se a bomba ainda não foi ligada;
- Se essas condições forem verdadeiras, a bomba é ligada por 30 segundos e a variável bomba_ligada é atualizada para True;
- Às 18:00, a variável bomba_ligada é redefinida para False para que a bomba possa ser ligada novamente no próximo dia às 17:00.

<b>*Compatibilidade e Integração Técnica:*</b>

A bomba de água, controlada pelo relé, exigiu um planejamento cuidadoso para garantir que o relé pudesse manejar a corrente necessária sem falhas. A configuração de horários específicos e tempo curto para a operação da bomba minimiza o consumo de energia e evita sobrecargas.
<br><br>
Um dos maiores desafios do projeto foi a sincronização de operações. A necessidade de sincronizar as operações dos relés com os LEDS, com tempos de acionamento da bomba de água, e captura de imagens demandou um controle preciso do tempo e eventos.

## **Sensor de nível de água horizontal tipo boia**

<figcaption style="text-align: center">
    <b>Figura 6: Sensor de nível de água horizontal tipo boia</b>
</figcaption>

<center>

![Sensor de nível de água horizontal tipo boia](assets/images/nivel.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: <a href="https://www.baudaeletronica.com.br/produto/sensor-de-nivel-de-agua-com-boia-horizontal.html"> Baú da Eletrônica - Sensor de Nível de Água com Boia Horizontal </a></small>
</figcaption>

O sensor foi escolhido por ser uma solução prática e simples para controlar o nível de água do reservatório inferior. O controle de nível no reservatório inferior é necessário para evitar que a bomba de água opere em vazio e sofra danos. Esse sensor é utilizado para controle de nível em aquários, tanques, recipientes de enchimento entre outros. Quando o líquido chega a um determinado nível, é acionado uma chave, que é utilizado para enviar a informação do nível do liquido a um dispositivo. Esse sensor é compatível com a maioria dos microcontroladores do mercado como PIC, Arduino e Raspberry.

A obtenção dos dados do nível da água foi realizado através do <a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/docs/-/blob/main/docs/assets/codigos/nivel_control.py?ref_type=heads">código</a> que funciona conforme o descrito abaixo: 
- É feita a configuração do GPIO da raspberry;
- O sensor_pin é configurado como uma entrada com um resistor pull-up interno. Isso garante que o pino seja "puxado" para 3.3V quando o sensor está aberto (sem contato);
-  É definida uma função a ser chamada quando o estado do sensor muda;
- Mantém o script rodando indefinidamente, monitorando o estado do sensor e respondendo a mudanças.

<b>*Compatibilidade e Integração Técnica:*</b>

A simplicidade desse sensor facilitou a integração, pois ele funciona como um interruptor que pode ser facilmente lido pela Raspberry Pi. No entanto, garantir a precisão e a confiabilidade do sensor em condições variáveis de operação foi um desafio superado com múltiplos testes e calibrações.


## **Modulo GPS USB**

<figcaption style="text-align: center">
    <b>Figura 7: Modulo GPS VK-172</b>
</figcaption>
<center>

![Modulo GPS](assets/images/gps_vk.png)

</center>
<figcaption style="text-align: center">
     <small>Fonte: <a href="https://www.lojaml.robobuilders.com.br/MLB-3517995733-modulo-gps-vk-172-usb-para-rasbperry-pi-arduino-nfe-_JM"> RoboBuilders - Módulo Gps Vk-172 Usb Para Rasbperry Pi Arduino </a></small>
</figcaption>

O modulo GPS foi escolhido pois utiliza conexão usb, evitando o uso a mais de "Jumpers" evitando o risco de desconectar durante o uso. Utiliza biblioteca GPSD para o sistema operacional Linux.

## **Sensor de temperatura Ds18b20**

<figcaption style="text-align: center">
    <b>Figura 7: Sensor de temperatura Ds18b20</b>
</figcaption>

<center>

![Sensor de temperatura Ds18b20](assets/images/sensortemp.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: <a href="https://www.baudaeletronica.com.br/produto/sensor-de-temperatura-a-prova-de-agua-ds18b20-1m.html"> Baú da Eletrônica - Sensor de temperatura a prova de água DS18B20 - 1m </a></small>
</figcaption>

O sensor foi escolhido pois utiliza uma interface presente na Raspberry, interface de comunicação de fio único que normalmente é utilizada no GPIO 4. Essa interface é extremamente simples e fácil de implementar. Esse dispositivo também apresenta uma faixa de operação muito boa entre -55 C° e +125 C°, como é esperado que o sistema opere entre 5 e 30° então está corretamente especificado. O sensor confirma a temperatura da água para pesquisadores que queiram esse dado, mas também para controle da temperatura do reservatório superior. Isso é necessário para que a água esteja em temperatura adequada para as larvas e isso leve a mais mosquitos depositarem ali.


Na etapa 2 do projeto foi implementado um <a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/docs/-/blob/main/docs/assets/codigos/temp_control.py?ref_type=heads">código</a> para extração dos dados do sensor que funciona da seguinte forma:
- É definido o diretório base onde os dispositivos 1-Wire são montados;
- Lê as linhas brutas do arquivo w1_slave que contém os dados do sensor;
- Processa as linhas para extrair a temperatura em graus Celsius;
- Lê a temperatura e a imprime no console a cada segundo.

<b>*Compatibilidade e Integração Técnica:*</b>

O sensor DS18B20 utiliza a interface de comunicação 1-Wire, que é suportada diretamente pela Raspberry Pi. Implementar a leitura do sensor foi facilitado pela disponibilidade de bibliotecas e suporte comunitário.

Todos os códigos citados neste documento estão disponíveis neste <a href="https://gitlab.com/lappis-unb/fga-pi2/semestre-2024-1/g1/docs/-/tree/main/docs/assets/codigos?ref_type=heads">link</a>.

<br>
<br>


## **Manutenção do sistema**


<b>*Plano de Manutenção:*</b>

<b>Inspeções Regulares:</b><br>
Frequência: Semanal <br>
Atividades: Verificar o nível de água, estado das baterias, funcionamento dos LEDs, bomba e sensores. Inspecionar a integridade da estrutura (plástico, tubos de PVC e alumínio).


<b>Limpeza:</b><br>
Frequência: Quinzenal<br>
Atividades: Limpeza da armadilha, troca da água e substituição do filtro para remover detritos e resíduos acumulados que possam interferir no funcionamento dos sensores e da câmera.


<b>Substituição de Componentes:</b><br>
Frequência: Conforme necessidade (baseado em inspeções)<br>
Atividades: Substituir baterias desgastadas, sensores ou LEDs defeituosos e componentes estruturais danificados.


<b>Atualização de Software:</b><br>
Frequência: Conforme necessário.<br>
Atividades: Verificar e instalar atualizações de software disponíveis para o aplicativo móvel e firmware dos componentes eletrônicos, garantindo a correção de bugs e a adição de novas funcionalidades.

<br>
<br>
<br>

## **Futuras Melhorias do Sistema**

<b>* Implementação de Sensores Adicionais:</b>

Adicionar novos sensores que possam monitorar outros parâmetros ambientais relevantes, como umidade relativa do ar que poderia ajudar a monitorar a incidencia de chuva na região. Um microfone que poderia identificar a especime do mosquitos apenas pelo som. Esses sensores iriam complementar as informaçoes adquiridas.

<b>* Aprimoramento da Conectividade:</b>

Implementação de conectividade LoRa para melhorar a comunicação de dados em áreas com cobertura de internet limitada.


<b>* Redundância e Resiliência:</b>

Introduzir redundâncias para componentes críticos, como a utilização de múltiplos sensores de nível de água e temperatura, garantindo que o sistema continue operando corretamente em caso de falha de um sensor. Tambem a implementaçao do sensor de tensão e corrente teve problemas, esse sensor seria muito importante avisando por exemplo quando o motor tivesse algum defeito.













## Histórico de versões

| Versão | Alteração                           | Data       | Autor              |
| ------ | ----------------------------------- | ---------- | ------------------ |
| 1.0    | Criação do documento e seu conteúdo | 26/04/2024 | Gabriel e Jhessica |
| 1.1    | Atualização do documento            | 30/05/2024 | Jhessica           |
| 1.2    | Atualização do documento            | 11/07/2024 | Jhessica           |
| 1.3    | Atualização do documento            | 12/07/2024 | Gabriel            |


