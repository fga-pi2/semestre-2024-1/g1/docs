O projeto de energia lista e explica o uso de componentes relacionados ao sistema de alimentação de energia elétrica do produto.

## Dimensionamento da Bomba

Inicialmente é necessário analisar o esquemático do projeto, como indicado na imagem abaixo:

<figcaption style="text-align: center">
    <b>Figura 1: Esquemático do projeto</b>
</figcaption>

<center>

![Esquemático do projeto](assets/images/Energia_VC2.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

Então, para o funcionamento do projeto, se faz necessário algum dispositivo para encher o reservatório superior com água quando necessário, com isso, foi levantado a perspectiva de uma bomba para essa tarefa, conforme a Figura 1. Com essa necessidade, a bomba precisa atender alguns requisitos, como:

- Baixo custo;
- Tamanho;
- Baixa demanda energética;
- Atender ao fluxo exigido de forma rápida.

Seguindo a premissa mais importante mencionada, a de atender ao fluxo, é necessário uma análise do reservatório, para assim, ser possível calcular outras características do fluxo de energia e de massa. Assim, é considerado o reservatório superior com 15 centímetros de diâmetro e 8,5 centímetros de altura, totalizando um volume de 1,5 litros.

A partir do volume, também é requisitado o tempo necessário para encher esse reservatório, levando em consideração que o tempo de operação interferirá diretamente na captura das larvas, por conta de ser indispensável a água em um regime sem perturbações. Com isso, é considerado um tempo máximo de operação sendo de até vinte segundos, de forma a satisfazer essa agilidade pretendida.

Com o volume a ser completo nesse tempo, é exequível a escolha da bomba de forma a satisfazer no mínimo o fluxo mássico estabelecido, conforme a equação de Bernoulli. Com essa vazão, é analisado o próximo requisito mais significativo, sendo o de demanda energética e de tensão de operação, tornando-se mais adequado uma bomba com esses valores baixos, pelo simples motivo de atender as necessidades sem atribuir grandes custos tanto de construção quanto o de operação, referente a demanda energética. Vale ser mencionado que esse aspecto se relaciona fortemente também com as premissas de tamanho e baixo custo.

Assim, foi procurado uma bomba que possa se adequar de forma mais abrangente a essas premissas, considerando as mais importantes. Com isso, foi selecionado uma bomba com as seguintes especificações:

A bomba não possui escovas e funciona como uma bomba centrífuga, sendo assim, possui palhetas que ao rodearem, constituem uma zona de alta pressão, sugando o fluido para uma zona destinada com pressão menor, como mostrado na derivação da equação de balanço de massa. Para isso, é considerando a massa específica da água tanto na entrada e saída como constante e o seguinte volume de controle:

<figcaption style="text-align: center">
    <b>Figura 2:  Volume de controle</b>
</figcaption>

<center>

![Volume de controle](assets/images/energia_VC3.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Equação da continuidade, vazão constante

```
0=ṁs-ṁe
Ve.A=.Vs.A
Ve=Vs
V=ṁA
```

Sendo:

**ṁs** a o vazão mássico de saída (em metros quadrados por segundo);

**ṁe** a o vazão mássico de entrada (em metros quadrados por segundo);

a massa específica da água (em massa por metros cúbicos);

**Vs** a velocidade do fluido na saída (em metros por segundo);

**Ve** a velocidade do fluido na entrada (em metros por segundo);

**A** a área de entrada ou de saída do fluido (em metros quadrados).

Assim, com a equação da continuidade, é possível se calcular o valor das velocidades nos eixos de entrada e de saída da bomba, levando em consideração que a área de entrada e de saída da bomba são iguais, logo a velocidade da vazão nos trechos são constantes, resultando no valor de 1,67 metros por segundo. Com essas velocidades, é possível utilizar a equação de balanço de energia para escoamentos, podendo se resumir para a equação de balanço de energia:

Equação de Bernoulli

```
W+Q+P1+V1²2+g.z1=P2+V2²2+g.z2
```

Sendo:

**W** o trabalho de eixo;

**Q** o calor gerado e transferido;

**P1** e **P2** a pressão de entrada e a pressão de saída, respectivamente (em Pascal);

**g** a aceleração da gravidade (em metros por segundo ao quadrado);

**z1** e **z2** a altura de entrada e altura de saída respectivamente (em metros).

Assim, considerando que possui diferença de altura entre a entrada e saída e velocidade constante, e que não tem calor no sistema, além da notória diferença de pressão já mencionada, é alcançado a seguinte equação de trabalho do eixo da bomba:

```
Weixo=(P2-P1)/ρ+g.(zi-z2)

```

Logo, é concluído que para o cálculo do trabalho de eixo do motor, é necessário mensurar a diferença de pressão atingida pela bomba entre o bocal de entrada e da saída. Essa diferença de pressão é essencial também para o cálculo de perda de carga do escoamento. A perda de carga está associada ao quanto de pressão é perdida ao longo do sistema hidráulico, conforme a seguinte equação:

```
Hf=ΔP/γ

```

Sendo

**Hf** o valor da perda de carga;

**γ** o peso específico,

Além disso, é possível analisar o regime de operação desse fluxo mássico através do valor do módulo de Reynolds, considerando tubulação com o mesmo diâmetro dos bocais de entrada e saída da bomba. Com esse número é possível interpretar outros parâmetros do escoamento.

```
Re=Vm.D/ν

```

Sendo

**Vm**a velocidade média do fluido (metros por segundo);

**D** o diâmetro da bomba (metros);

**v** o volume específico (m³/kg)

Dessa forma, o módulo de Reynolds para o sistema é de 21,7, assim é possível classificar como Regime Laminar, também chamado de escoamento viscoso. Esse tipo de regime se caracteriza pelo fluido descrever trajetórias insignificantes variáveis, isso se deve principalmente pela baixa velocidade do fluido.

Sabendo que o regime do escoamento é laminar, é possível se calcular o fator de atrito de forma simplificada, através da seguinte fórmula empírica, diferente do cálculo se o regime fosse turbulento.

```
f=64/Re
```

Assim o fator de atrito é 2,94 esse fator está relacionado a rugosidade interna da tubulação. Vale ser mencionado que por o regime ser laminar, não é necessário a análise do fator de atrito com base no material da tubulação, e consequentemente sendo dispensável essa análise com o gráfico de Moody, que considera a rugosidade relativa do tubo e perdas de cargas localizadas.

Além disso, a bomba possui uma corrente de operação estimada de 910 mA, calculada por lei de Ohm, funcionando no regime de corrente contínua, e tendo especificação de 1A de corrente nominal. Importante mencionar que essa bomba funciona com o accionamento de um motor acoplado a um rotor, girando o eixo de forma a gerar essa sucção de fluido. O motor funciona por conta da passagem da corrente contínua pelas espiras, que consequentemente geram um campo eletromagnético no estator do motor, desencadeando o movimento rotativo do rotor.

## Subsistema de Alimentação

Para a definição do serviço de alimentação, é necessário estimar a demanda energética de todos os outros subsistemas, sendo importante aferir os equipamentos utilizados e sua respectiva potência de operação. Além disso, como a maior parte da potência do sistema funciona de forma intermitente, de forma a depender da frequência de ativação da bomba, torna-se fundamental analisar a periodicidade de ativação do sistema, analisando de forma mais detalhada o desenvolvimento biológico da larva depositada.

Com isso, para primeira avaliação do quanto será demandado de energia, foi ponderado inicialmente a alimentação feita de forma isolada. Assim, para ser levantado a incidência de acionamento da bomba mais a demanda dos sensores, foi considerado o processamento do equipamento duas vezes por dia. Deve ser considerado também para a análise da demanda o consumo elétrico dos sensores durante o período de dois minutos a cada duas horas, em cada dia.

### Dimensionamento da carga e demanda energética

Pensado para o sistema isolado, foi escolhido prosseguir com baterias para proporcionar uma corrente contínua, que será alimentada por uma placa fotovoltaica, que alimentará a bateria logo depois da passagem pelo controlador de carga solar. Esse sistema consegue atender a necessidade de não estar conectado na rede para o funcionamento, nem necessidade de um carregamento manual das baterias.

Com esse planejamento, se faz necessário saber também o tempo de funcionamento dos componentes e sua potência demandada por dia, para assim, levantar se a configuração das baterias escolhidas suportam, mesmo se houver falha do sistema de geração, considerando problemas de intermitência climática, como períodos sem irradiação. Como é desconhecido a frequência que os mosquitos depositam no OvniTrap, realizamos uma estimativa do quanto de energia seria necessário para realizar a operação de fluxo de água durante o máximo de dias inteiros que a bateria escolhida suporta, sem considerar sua alimentação. Com isso, foi atingido o máximo de sete dias de funcionamento, considerando um consumo de energia de 3,8Wh em um dia, conforme tabela abaixo.

<figcaption style="text-align: center">
    <b>Tabela 1: Demanda energética dos componentes </b>
</figcaption>
<center>

| Equipamento              | Voltagem de operação (Volts) | Corrente de operação (mA) | Tempo de funcionamento em um dia (horas) | Energia demandada (Watts hora) |
| ------------------------ | ---------------------------- | ------------------------- | ---------------------------------------- | ------------------------------ |
| Bomba                    | 24                           | 910                       | 0,011                                    | 0,242                          |
| Raspberry pi             | 5                            | 950                       | 0,4                                      | 1,9                            |
| Módulo Relé Relay        | 10                           | 15                        | 0,4                                      | 0,06                           |
| Sensor De Distância      | 5                            | 2                         | 0,4                                      | 0,004                          |
| 6x Módulo Led Lilypad    | 30                           | 5                         | 0,4                                      | 0,06                           |
| 6 Led 5mm Alto Brilho UV | 3                            | 20                        | 0,4                                      | 0,024                          |
| Sensor de temperatura    | 5                            | 5                         | 0,4                                      | 0,01                           |
| 3 Led 5mm Alto Brilho UV | 30                           | 5                         | 0,4                                      | 0,06                           |
| 6x Módulo Led Lilypad    | 30                           | 5                         | 0,4                                      | 0,06                           |
| Sim808 Módulo Gsm Gprs   | 4                            | 50                        | 0,4                                      | 0,08                           |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

As baterias escolhidas foram dimensionadas a partir da voltagem máxima de operação do projeto, ou seja, a diferença de diferencial da bomba. Com isso, foi ponderado a escolha de duas baterias conectadas em serie que apresentem em conjunto 24V e possuindo o mínimo de amperagem hora para o funcionamento do sistema.

Importante destacar que além da potência dimensionada para atender a demanda do projeto, também foi considerado a escolha de corrente máxima de operação do sistema, se limitando a 1,3 ampéres, conferindo menores perdas por efeito Joule. Foi analisado para a escolha da bateria se é emitido algum gás durante sua operação, tendo uma quantidade insignificante, aspecto importante considerado por conta de necessitar ser inerte ao ambiente com seres vivos.

Adicionalmente, foi necessário acoplar um sensor de medição de voltagem e corrente, de forma a ser possível monitorar o nível de potência das baterias. O módulo escolhido para exercer essa função é o INA3221, além de garantir o funcionamento desejado, é compatível para utilizar junto a Raspberry, necessitando ser adicionado algumas bibliotecas de código para seu funcionamento.

**Sistema de geração**

Como mencionado, para o funcionamento contínuo do projeto sem a necessidade de ter um carregamento manual das baterias, foi escolhido seguir com as baterias acopladas em um sistema de geração off-grid, com placas fotovoltaicas. Para isso, se faz necessário uma análise do quanto será consumido para ser “abatido” do quanto deverá ser gerado para o dimensionamento fotovoltaico.

Para isso, é necessário inicialmente ser definido qual placa será utilizada, para então se analisar quantas serão necessárias para alimentar o sistema sem que falte energia. Assim, de forma a tornar a implementação do projeto mais viável, foi cedido uma placa de 50 centímetros por 50 centímetros da marca Kyocera, que possui potência de 45 watts.

Também é essencial ser analisada a incidência solar na região a ser utilizada, sendo definido inicialmente na região de Brasília, mais especificamente para o campus da Universidade de Brasília no Gama. Para isso, foram verificados esses dados no Centro de Referência para as Energias Solar e Eólica Sérgio de S. Brito (CRESESB), usando como referências de coordenadas com latitude: -15,98961 e longitude: -48,04439. Assim, foi recolhido o dado da irradiação solar média diária de 5,25 kWh/m². Com esse valor, podemos estimar a produção de energia necessária com base na eficiência de conversão da placa sendo 15%, da seguinte forma:

```
Pss=E/Is.n
```

Sendo:

**Pss** a Potência estimada do sistema solar (em quiloWatts);

**E** a energia demandada do projeto em um dia (em quiloWatts horas);

**Is** a irradiação média solar no local (em horas);

**n** a eficiência da placa solar (em porcentagem).

Utilizando o valor total de energia calculado na Tabela”Demanda energética diária”, sendo como 3,9 Watts hora, a irradiação recolhida sendo 5,25 e a eficiência da placa como 20%, é resultado como potência estimada de 4,8 Watts. Assim, é possível analisar que uma única placa é mais do que suficiente para alimentar o sistema como inteiro.

Importante mencionar que o projeto inteiro funciona em corrente contínua, não necessitando de um microinversor. Entretanto, para gerir de forma mais segura a geração e consumo de energia de todo o sistema, foi selecionado um controlador de carga, de forma a suavizar a geração e fazer esse controle de corrente, impedindo o fluxo de corrente reversa quando a bateria ficar com um potencial maior do que o da placa, como de noite. Assim, levando em consideração o tamanho do projeto, foi optado pelo controlador de carga do tipo PWM LCD, que faz esse ajuste de potência, mantendo a corrente com o mesmo valor de entrada.

### Dimensionamento dos condutores

Para o dimensionamento dos condutores elétricos do sistema de alimentação, foi estruturado principalmente para atender a Associação Brasileira de Normas Técnicas - ABNT 5410, que tem como principal função adequar as instalações elétricas para sistemas de baixa tensão. Com isso, foi necessário avaliar a corrente de operação do sistema.

Continuamente para a escolha do condutor, basta buscar na tabela 33 da norma mencionada, o tipo de linha elétrica do projeto, sendo a mais apropriada para o OvniTrap, o método C. Com a amperagem máxima definida e o método de linha determinado, deve ser buscado na tabela 37 da mesma norma, a seção transversal mínima para o projeto. Assim, foi estabelecida a seção nominal do cabo de 0,5 milímetros quadrados. Vale ser mencionado que esse cabeamento suporta corrente de até 12 amperes, sendo a mais próxima para suportar a necessidade do projeto.

Importante destacar que o cabeamento não passará por eletroduto, não possui fator de agrupamento, além disso, sendo então recomendado ter uma isolação extra, principalmente pelo projeto trabalhar com fluxo de água, estando sujeito a umidade maior. Com isso, foi preferido a isolação de borracha etileno-propileno - EPR, sendo adequado para esse tipo de projeto.

## Histórico de versões

| Versão | Alteração                            | Data       | Autor                 |
| ------ | ------------------------------------ | ---------- | --------------------- |
| 1.0    | Documentação do projeto de energia   | 27/04/2024 | Miguel Meira Wichoski |
| 2.0    | Correção de valor                    | 02/05/2024 | Miguel Meira Wichoski |
| 3.0    | Correção de valores e perda de carga | 27/05/2024 | Miguel Meira Wichoski |
| 4.0    | Sensor de voltagem                   | 02/06/2024 | Miguel Meira Wichoski |
