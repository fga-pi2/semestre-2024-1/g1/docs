## Projeto de Estrutura

A estrutura do Ovnitrap foi pensada para ter eficácia na captura de larvas, de forma barata, com alta durabilidade, de fácil montagem e estéticamente agradável. A escolha do plástico, dos tubos PVC e do alumínio foi motivada pela sua resistência à corrosão causada pela água, uma vez que o dispositivo envolve o armazenamento e ficará em ambiente suscetível a chuvas. Além disso, esses materiais, quando desmontados, são leves e de fácil manuseio, facilitando a montagem e o transporte do Ovnitrap.

O balde retangular de 20 litros servirá para armazenar o líquido de atração de mosquitos, utilizado no Ovnitrap, além de ser o suporte que sustentará todo o resto do equipamento. Em sua tampa terá uma placa de madeira para melhor resistência, onde será fixado um cano PVC principal. Dentro do cano principal, no seu cap, haverá um furo que servirá como entrada para os mosquitos, onde passará uma mangueira que levará água para um recipiente de PVC secundário que armazenará as larvas por meio de uma tela mosquiteiro. Na parte superior do cano PVC principal serão instalados LEDs UV para facilitar na atração dos mosquitos Aedes Aegypti, uma vez que a luz UV tem espectro de onda que correspondem aos picos de sensibilidade da maioria dos mosquitos Aedes [(1)](projeto-estrutura.md#referencias), tais luzes também servirão como fonte de luz para que seja captado fotos por meio de uma câmera.

No interior do balde será instalada uma mini bomba de água que levará o líquido de atração até a superfície do cano principal, ligadas por mangueiras.

Em paralelo à estrutura do balde, será feita uma estrutura de perfis de alumínio em formato de L, sendo uma estrutura para acomodar a placa solar e suas baterias, que gerará energia para o equipamento. 

Atrelado à estrutura do balde, será acoplada uma caixa de isopor contendo uma Raspberry Pi e principais cabos de alimentação, que ligam-na aos outros eletrônicos (câmera, LEDs, bomba d'água). Dessa forma, a armadilha poderá fazer a captura eficiente das larvas de mosquitos, garantindo a proteção dos dispositivos eletrônicos.

Por fim, toda estrutura será acoplada em um carrinho feito de metalon, para facilitar a locomoção com mesmo, permitindo que o usuário não carregue peso ou tenha mais dificuldade.

Na figura abaixo é possível ver o desenho de todas as peças CAD, além de seus respectivos materiais, feito no CATIA V5. Os demais desenhos técnicos de cada parte estrutural poderão ser encontrados nos apêndices do projeto.

<figcaption style="text-align: center">
    <b>Figura 1: Desenho Técnico da parte estrutural do projeto</b>
</figcaption>

<center>

![Desenho Técnico da parte estrutural do projeto](assets/images/estrutura_completa.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Diagrama Hidráulico

O diagrama hidráulico apresentado na figura abaixo ilustra o percurso da água no Ovnitrap. No canto inferior do balde, há uma bomba que impulsiona a água para um reservatório principal de PVC, por meio de uma mangueira de 1/2 polegada. Este reservatório principal funcionará em dois tempos:

- Tempo 1: o reservatório será enchido com água até abaixo da altura da mangueira de 1/4 de polegada.
- Tempo 2: a bomba será religada e o reservatório será enchido até ultrapassar o nível da mangueira de 1/4 de polegada.

Ao ultrapassar esse nível, a bomba será desligada e entrará em funcionamento o princípio do copo de Pitágoras. O copo de Pitágoras é um dispositivo que usa um tubo interno para criar um sifão. Quando o nível da água no copo atinge a altura do tubo, o sifão entra em ação e drena todo o líquido do copo através da força da gravidade. Logo após, a mangueira menor levará o líquido junto com as larvas para o reservatório de PVC secundário, onde as larvas ficarão presas até a morte.

<figcaption style="text-align: center">
    <b>Figura 2: Diagrama Hidráulico</b>
</figcaption>

<center>

![Diagrama Hidráulico](assets/images/diagrama-hid.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Equilíbrio da Estrutura

<!-- **Força de atrito que impede a movimentação do balde:**
$$
\text{Fat} = m \cdot N
$$
sendo \( m \) o coeficiente de atrito, adotado como 0,5 (atrito do plástico com solo de concreto).
A força \( N \), normal, é igual ao peso, sendo a massa vezes a gravidade, logo: 
$$
N = 10\text{kg} \times 9,8 = 98N
$$
$$
\text{Fat} = m \cdot N = 0,5 \times 98 = 49 N
$$

Portanto, o vento precisa ultrapassar uma força de **49N** para quebrar o atrito e mover o balde.

**Fórmula para calcular a pressão dinâmica do vento:**
$$
p = 0,5 \times \text{densidade do vento} \times \text{velocidade do vento}^2 
$$
Sendo a densidade do vento de 1,25 e levando em consideração um vento médio a velocidade de 22,1m/s:
$$
p = 0,5 \times 1,25 \times 22,1^2 = 305,256N/m²
$$
$$
F = \text{área} \times p 
$$
Área da superfície do balde em contato: 
$$
A = 0,32 \times 0,23 = 0,074m^2
$$
que deve ser multiplicado pelo coeficiente de arrasto, que será 2,2 visto que a superfície é um quadrado [(3)](projeto-estrutura.md#referencias). Logo: 
$$
A = 0,1619m^2
$$
$$
F = 0,1619 \times 305,256 = 49,43N 
$$ -->

<figcaption style="text-align: center">
    <b>Figura 3: Cálculo do equilíbrio estrutural</b>
</figcaption>

<center>

![Cálculo do equilíbrio estrutural](assets/images/equilibrio-estrutura.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

> Observação: Foi realizado uma captura da tela devido não ser possível utilizar fórmulas matemáticas no GitLab Pages.

Sendo assim, a velocidade necessária para o vento mover o balde é de **22,1 m/s = 79,56 km/h**. Não necessitando de uma estrutura complementar para assegurar o equilíbrio, visto que a média de vento no Brasil é de no máximo **8 m/s** [(2)](projeto-estrutura.md#referencias).


### Montagem Final

Na figura abaixo é possível ver a montagem completa da estrutura, em sua forma pronta para funcionamento.

<figcaption style="text-align: center">
    <b>Figura 4: Estrutura do Ovnitrap</b>
</figcaption>

<center>

![Estrutura do Ovnitrap](assets/images/estrutura-montada.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Referências

> 1. Müller, N.F.D. _A tratividade de diferentes armadilhas luminosas para coleta de mosquitos (Diptera: Culicidae) e artrópodes não alvo em Cachoeirinha, RS_. Universidade Federal do Rio Grande do Sul, 2019. Disponível em: https://lume.ufrgs.br/handle/10183/237409.

> 2. CRESESB. BRASIL POTENCIAL EÓLICO. Centro de Referência para Energia Solar e Eólica, 2001. Disponível em: www.cresesb.cepel.br/publicacoes/download/atlas_eolico/mapas_1a.pdf.

> 3. BARBOSA, J. Cálculo da Velocidade do Vento Limite na Movimentação de Carga. Consultoria Engenharia, 2017. Disponível em: https://consultoriaengenharia.com.br/seguranca-ocupacional/calculo-da-velocidade-do-vento-limite-na-movimentacao-de-carga/.

## Histórico de versões

| Versão | Alteração            | Data       | Autor      |
| ------ | -------------------- | ---------- | ---------- |
| 1.0    | Criação do documento | 27/04/2024 | Carlos Vaz |
| 1.1    | Correções gerais     | 31/05/2024 | Carlos Vaz |
| 1.2    | Correções gerais     | 11/07/2024 | Carlos Vaz |