# Projeto de Software

Para o projeto de software serão utilizadas diversas tecnologias já conhecidas no mercado. A escolha das mesmas, bem como as ferramentas auxiliares visam o desenvolvimento de uma aplicação voltada à dispositivos móveis, tal aplicação tem como finalidade o monitoramento da armadilha de mosquito e suas informações.

## Frontend

### React Native

A ferramenta React Native será utilizada no desenvolvimento do Frontend. O React Native foi criado pelo Facebook em 2015 e é mantido até hoje. Em 2018, a ferramenta ocupou o segundo lugar em número de contribuidores no GitHub.[(1)](projeto-software.md#referencias)

Os motivos da escolha da ferramenta foram :

- Desenvolvimento Multiplataforma
- Eficiência de Desenvolvimento - Javascript e React
- Comunidade Ativa
- Biblioteca vasta de componentes e plataformização.

## Backend

### Django

Django é um framework de alto nível para desenvolvimento Web que aumenta a velocidade de desenvolvimento com um design limpo e programático. A linguagem de programação utilizada é Python e a ferramenta cuida de vários níveis de abstração, sem precisar reinventar a roda.[(2)](projeto-software.md#referencias)

Motivos para a escolha da ferramenta :

- Django é conhecido por sua sintaxe limpa e simples, além de sua velocidade.
- Segurança : Django possui recursos de segurança incorporados, como proteção contra ataques de injeção de SQL, proteção CSRF.
- Escalabilidade : Django suporta autoescalabilidade, conseguindo lidar sozinho com altas demandas de tráfego.

### SQLite

SQLite é um banco de dados relacional que se distingue de outras ferramentas similares por não requerer um servidor para armazenar informações. Ele alcança essa independência ao permitir que os dados sejam incorporados diretamente em seus arquivos. Sendo uma solução de código aberto e gratuita, o SQLite é amplamente adotado em aplicações móveis. [(3)](projeto-software.md#referencias)

## Modelo de reconhecimento de imagem

A capacidade de reconhecimento de imagem, frequentemente conhecida como visão computacional, é um recurso tecnológico que tem como base a inteligência artificial (IA). Nesse sentido, a IA é capaz de discernir, identificar e classificar uma ampla gama de imagens, abrangendo desde fotografias de plantas e objetos até produtos e rostos humanos. Essa tecnologia é amplamente usada em diversos setores, como medicina, transporte e comércio eletrônico, e vem ganhando cada vez mais espaço na sociedade por sua utilidade e eficiência. [(4)](projeto-software.md#referencias)

O reconhecimento de imagem funciona à medida que a inteligência artificial é treinada com um conjunto de imagens de referência. A partir delas, ela começa a ser treinada para identificar características semelhantes e distintas entre as imagens analisadas, para depois conseguir identificar, comparar e classificar as imagens de acordo com o aprendizado. [(4)](projeto-software.md#referencias)

O YOLO é um método de detecção de objetos de passagem única (single pass) que utiliza uma rede neural convolucional como extrator de características. A principal vantagem do YOLO em relação aos outros métodos é que ele realiza as predições de classe em uma única passagem pela rede. Ao contrário dos sistemas anteriores de detecção de objetos, que dividiam a imagem em várias regiões e executavam um classificador em cada uma delas, o YOLO simplifica o processo ao processar a imagem inteira de uma vez. [(4)](projeto-software.md#referencias)

## Diagrama de Pacotes

O diagrama de pacotes é uma forma de modelagem estática utilizada para representar de forma geral a estrutura de um projeto, proporcionando uma visão organizacional da arquitetura do sistema valendo-se da notação: _Unified Modeling Language_ (UML) . Abaixo é possível observar os diagramas correspondentes aos diagramas de pacotes do sistema Ovnitrap.

<figcaption style="text-align: center">
    <b>Figura 1: Diagrama de Pacotes - Visão Geral</b>
</figcaption>

<center>

![Diagrama de Pacotes - Visão Geral](assets/images/visao-geral-diagrama-pacotes.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Front-end

<figcaption style="text-align: center">
    <b>Figura 2: Diagrama de Pacotes - Front-end</b>
</figcaption>

<center>

![Diagrama de Pacotes - Front-end](assets/images/frontend-diagrama-pacotes.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Back-end

<figcaption style="text-align: center">
    <b>Figura 3: Diagrama de Pacotes - Back-end</b>
</figcaption>

<center>

![Diagrama de Pacotes - Back-end](assets/images/backend-diagrama-pacotes.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Modelagem do Banco de Dados

### Modelo Entidade-Relacionamento (ME-R)

#### Entidades

- ARMADILHA
- RELATORIO

#### Atributos das Entidades

- ARMADILHA (idArmadilha, apelido)
- RELATORIO (idRelatorio, nivelBateria, nivelAgua, temperaturaAgua, latitude, longitude, dataHora, qtdLarvas, imagem, dataHoraDescarga)

#### Relacionamentos

ARMADILHA - gera - RELATORIO (1:n)
Uma ARMADILHA gera zero ou mais RELATORIO, e um relatório é gerado por uma ARMADILHA.

### Diagrama Entidade-Relacionamento (DE-R)

<figcaption style="text-align: center">
    <b>Figura 4: Diagrama Entidade-Relacionamento (DE-R)</b>
</figcaption>

<center>

![Diagrama Entidade-Relacionamento (DE-R)](assets/images/Conceitual.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### Diagrama Lógico de Dados

<figcaption style="text-align: center">
    <b>Figura 5: Diagrama Lógico de Dados</b>
</figcaption>

<center>

![Diagrama Lógico de Dados](assets/images/Logico.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Diagrama de Classes ORM

<figcaption style="text-align: center">
    <b>Figura 6: Diagrama de Classes ORM</b>
</figcaption>

<center>

![Diagrama de Classes ORM](assets/images/diagrama-classe.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Diagrama de fluxo de exceção

O diagrama de fluxo de execução apresentado é um modelo de BPMN (Business Process Model and Notation) que descreve o processo de monitoramento e notificação utilizando sensores.

<center>

![Diagrama de Pacotes - Visão Geral](assets/images/fluxograma%20-%20ovnitrap%20-%20Frame%201.jpg)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

### 1. Início
O processo se inicia com a detecção do ligamento da rasp.

### 2. Verificação de Conexão

Se Não, verifica se o dispositivo está Conectado ao Servidor?

1) Se Não, registra a falha e informa o responsável pela falha na conexão.

2) Se Sim, atualiza a data e a hora do sistema.

### 3. Leitura dos Sensores:

O sistema lê os dados de diversos sensores:

- Leitura Sensor de Tensão (x -  não implementado)
- Leitura Sensor de Temperatura
- Leitura Sensor de Humidade
- Leitura de Bateria do Sistema (x -  não implementado)
- Leitura de Nível de Sinal

### 4. Verificação de Alertas

Alerte Disparo de Anomalia?

1) Se Sim, envia os dados para o servidor e registra os eventos de anomalia.

2) Se Não, verifica se houve leitura dentro dos padrões esperados

### 5. Finalização

O processo é finalizado, se necessário, com a notificação do "alarme" de desconexão ou desligamento do sistema.


## Referências

<section id="referencias">
   
</section>

> 1. React Native. Meta. Disponível em: https://reactnative.dev/. Acesso em: 25 de abril de 2024.

> 2. Django. Disponível em: https://www.djangoproject.com/. Acesso em: 25 de abril de 2024.

> 3. SOUZA, Ivan de. O que é SQLite, por que ele é usado, e o que o diferencia do MySQL? 24 nov. 2020. Disponível em: https://rockcontent.com/br/blog/sqlite/

> 4. ALVES, Gabriel. Detecção de objetos com YOLO: uma abordagem moderna. 13 out. 2020. Disponível em: https://iaexpert.academy/2020/10/13/deteccao-de-objetos-com-yolo-uma-abordagem-moderna/. Acesso em: 28 abr. 2024.

## Histórico de versões

| Versão | Alteração                                                                       | Data       | Autor                    |
| ------ | ------------------------------------------------------------------------------- | ---------- | ------------------------ |
| 1.0    | Criação do documento com a versão 1                                             | 24/04/2024 | Luan Vasco               |
| 1.1    | Correções no documento                                                          | 28/04/2024 | Kayro César              |
| 1.2    | Primeira versão do banco de dados                                               | 17/05/2024 | Luís Lins e Victor Yukio |
| 1.3    | Reestruturação do documento e adição do diagrama de pacotes geral e do back-end | 30/05/2024 | Lucas Gabriel            |
| 1.4    | diagrama de fluxo de exceção                                              | 17/05/2024 | Luís Lins e Victor Yukio |
