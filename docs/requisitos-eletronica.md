A seguir estão elucidados os requisitos funcionais e requisitos não funcionais, respectivamente nas Tabela 1 e 2, relativos ao subsistema de eletrônica do Ovnitrap.

## Requisitos Funcionais

<figcaption style="text-align: center">
    <b>Tabela 1: Requisitos funcionais de eletrônica</b>
</figcaption>
<center>

| Requisitos Funcionais | Descrição                                                                                                                           |
| :-------------------: | :---------------------------------------------------------------------------------------------------------------------------------- |
|          RF1          | O sistema deve ser capaz de monitorar através de imagens a presença de larvas na água estagnada.                                    |
|          RF2          | Após determinado período de estagnação, o sistema deve acionar automaticamente uma válvula para escoamento da água do reservatório. |
|          RF3          | O sistema deve reter as larvas presentes na água durante o escoamento por meio de um filtro.                                        |
|          RF4          | Após o escoamento da água, o sistema deve acionar uma bomba de aquário para reabastecer o reservatório.                             |
|          RF5          | O sistema deve ser capaz de monitorar continuamente o nível da água no reservatório.                                                |
|          RF6          | O sistema deve ser capaz de monitorar a temperatura da água.                                                                        |
|          RF7          | O sistema deve ser capaz de monitorar continuamente o nível da bateria.                                                             |
|          RF8          | O sistema deve ser capaz de fornecer a localização exata de cada uma das armadilhas.                                                |
|          RF9          | Os dados coletados (imagens, nível da água, etc.) devem ser armazenados e enviados para análise posterior.                          |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Requisitos não funcionais

<figcaption style="text-align: center">
    <b>Tabela 2: Requisitos não funcionais de eletrônica</b>
</figcaption>
<center>

| Requisitos Não Funcionais | Descrição                                                                                                                                    |
| :-----------------------: | :------------------------------------------------------------------------------------------------------------------------------------------- |
|           RNF1            | O consumo de energia do sistema deve ser otimizado para garantir uma vida útil prolongada da bateria ou fonte de alimentação.                |
|           RNF2            | O sistema deve ser resistente à água e projetado para operar em condições ambientais adversas, como alta umidade e variações de temperatura. |
|           RNF3            | O sistema deve atender aos padrões de segurança elétrica e de materiais aplicáveis, garantindo a proteção do usuário e do meio ambiente.     |
|           RNF4            | A manutenção do sistema deve ser simples, com peças de reposição facilmente disponíveis no mercado.                                          |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Histórico de versões

| Versão | Alteração                                       | Data       | Autor    |
| ------ | ----------------------------------------------- | ---------- | -------- |
| 1.0    | Criação do documento e definição dos requisitos | 26/04/2024 | Jhessica |
