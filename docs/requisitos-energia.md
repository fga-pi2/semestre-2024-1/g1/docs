A seguir estão elucidados os requisitos funcionais e requisitos não funcionais, respectivamente nas Tabela 1 e 2, relativos ao subsistema de energia do Ovnitrap.

## Requisitos Funcionais

<figcaption style="text-align: center">
    <b>Tabela 1: Requisitos funcionais de energia</b>
</figcaption>
<center>

| Requisito | Descrição                                                                                                                  |
| :-------: | -------------------------------------------------------------------------------------------------------------------------- |
|    RF1    | O produto deve possuir uma fonte de tensão adequada para os componentes.                                                   |
|    RF2    | O projeto deve possuir armazenamento de energia o suficiente para alimentar os componentes durante um período determinado. |
|    RF3    | O projeto deve possuir uma fonte de energia elétrica que atenda a demanda energética.                                      |
|    RF4    | O projeto deve possuir uma geração de energia, de forma a não depender de estar conectado na rede elétrica e nem necessitar de trocas frequentes de bateria.                                      |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Requisitos não funcionais

<figcaption style="text-align: center">
    <b>Tabela 2: Requisitos não funcionais de energia</b>
</figcaption>
<center>

| Requisito | Descrição                                                                                                                                          |
| :-------: | -------------------------------------------------------------------------------------------------------------------------------------------------- |
|   RNF1    | Autonomia, a fonte de energia deve ser capaz de atender o projeto de forma a não ser necessário troca de bateria ou carregamento na rede elétrica. |
|   RNF2    | O cabeamento do armazenamento e fonte de energia devem ser projetados de forma a considerar operações com alta umidade e variações de temperatura. |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Histórico de versões

| Versão | Alteração            | Data       | Autor        |
| ------ | -------------------- | ---------- | ------------ |
| 1.0    | Criação do documento | 26/04/2024 | Miguel Meira |
