Para a concepção da estrutura do Ovnitrap, é crucial que os requisitos sejam cumpridos para que atendam o escopo do projeto. Esses requisitos são fundamentais para garantir que a estrutura do Ovnitrap atenda às necessidades de desempenho, durabilidade e praticidade necessárias para sua eficácia e usabilidade. Nas tabelas 1 e 2 abaixo, são listados os requisitos funcionais e não funcionais, respectivamente, em termos do subsistema de estrutura.

## Requisitos Funcionais

<figcaption style="text-align: center">
    <b>Tabela 1: Requisitos funcionais de estrutura</b>
</figcaption>
<center>

| Requisito | Descrição                                                                                  |
| :-------: | :----------------------------------------------------------------------------------------- |
|    RF1    | A estrutura deve ser resistente o suficiente para suportar o peso do dispositivo completo. |
|    RF2    | Deve fornecer proteção adequada para os componentes internos.                              |
|    RF3    | A estrutura deve ser projetada para facilitar a montagem e desmontagem do dispositivo.     |
|    RF4    | Deve permitir fácil acesso aos componentes internos para manutenção e limpeza.             |
|    RF5    | Deve ser construída com materiais duráveis e resistentes às condições ambientais.          |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Requisitos não funcionais

<figcaption style="text-align: center">
    <b>Tabela 2: Requisitos não funcionais de estrutura</b>
</figcaption>
<center>

| Requisito | Descrição                                                                           |
| :-------: | :---------------------------------------------------------------------------------- |
|   RNF1    | A estrutura deve ser leve para facilitar o transporte e instalação.                 |
|   RNF2    | Deve ser dimensionada para ocupar o menor espaço possível.                          |
|   RNF3    | Deve ser resistente à corrosão devido à exposição a ambientes externos.             |
|   RNF4    | A estrutura deve ser esteticamente agradável, utilizando materiais de fácil acesso. |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Histórico de versões

| Versão | Alteração                            |    Data    |   Autor    |
| :----: | ------------------------------------ | :--------: | :--------: |
|  1.0   | Definição de requisitos de estrutura | 25/04/2024 | Carlos Vaz |
