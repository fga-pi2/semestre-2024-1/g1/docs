A seguir estão elucidados os requisitos funcionais e requisitos não funcionais, respectivamente nas Tabela 1 e 2, relativos ao subsistema de software do Ovnitrap.

## Requisitos Funcionais

<figcaption style="text-align: center">
    <b>Tabela 1: Requisitos funcionais de software</b>
</figcaption>
<center>

| Requisito | Descrição                                                                            |
| :-------: | :----------------------------------------------------------------------------------- |
|    RF1    | Deve ser possível conectar o aplicativo a uma armadilha específica                   |
|    RF2    | Deve ser possível visualizar o nível de água da armadilha                            |
|    RF3    | Deve ser possível visualizar a temperatura da água da armadilha                      |
|    RF4    | Deve ser possível visualizar o nível da bateria da armadilha                         |
|    RF5    | Deve ser possível visualizar o nível de atividade larval                             |
|    RF6    | Deve ser possível gerar um relatório com os indicadores da armadilha                 |
|    RF7    | Deve ser possível visualizar a imagem de detecção larval associada em cada relatório |
|    RF8    | Deve ser possível visualizar no mapa de calor a atividade larval de cada armadilha   |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Requisitos não funcionais

<figcaption style="text-align: center">
    <b>Tabela 2: Requisitos não funcionais de software</b>
</figcaption>
<center>

| Requisito | Descrição                                                                                 |
| :-------: | :---------------------------------------------------------------------------------------- |
|   RNF1    | O software deve ser desenvolvido para ambiente mobile, sendo compatível com IOS e Android |
|   RNF2    | O software deve ter uma interface simples e intuitiva                                     |
|   RNF3    | O software deve ter feedback claro para ações do usuário                                  |
|   RNF4    | O software deve realizar conexão com a armadilha via requisição HTTP                      |

</center>
<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Lista É /Não É

**É:**                                                                       

- Aplicativo voltado para análise de dados o mosquito da dengue na região 
- Aplicativo Mobile

**Não é:**

- Aplicação Web                            
- Aplicação de denúncia de focos de dengue 
- Não é uma rede social                    
- Plataforma para diagnóstico de dengue    

## Lista Faz / Não Faz

**Faz:**

- Indica nível de água da armadilha                    
- Indica temperatura da água                           
- Relatório mensurando a atividade larval na armadilha 
- Indica nível do bateria da armadilha                 
- Identifica presença de larvas                        

**Não Faz:**

- Controle de armadilha remoto                 
- Cadastro de novas armadilhas pelo aplicativo 

## Histórico de versões

| Versão | Alteração                           | Data       | Autor                                   |
| ------ | ----------------------------------- | ---------- | --------------------------------------- |
| 1.0    | Definição de requisitos de software | 25/04/2024 | Lucas Gabriel, Kayro César e Luan Vasco |
| 1.1    | Ajuste nos requisitos de software   | 27/04/2024 | Lucas Gabriel, Kayro César e Luan Vasco |
