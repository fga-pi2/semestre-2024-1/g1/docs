Os requisitos são uma parte fundamental para uma boa concepção de projeto, pois traduzem as necessidades do cliente em especificações para o sistema. Fornecendo descrições do que o sistema deve fazer e de suas restrições. Desse modo, os requisitos correspondem a uma declaração abstrata de alto nível de um serviço ou uma restrição sobre o sistema, geralmente esses são classificados em duas categorias: requisitos funcionais e requisitos não funcionais [(1)](requisitos.md#referencias).

Os requisitos funcionais descrevem as ações que serão realizadas pelo sistema, como o sistema deve reagir a um determinado input e como o sistema deve se comportar em determinadas situações, já os requisitos não funcionais estão associados a restrições sobre os serviços oferecidos pelo sistema, podendo incluir restrições como limite de tempo para determinadas ações, restrições no processo de desenvolvimento e restrições associadas a adesão de padrões e normas, sendo esses geralmente aplicados ao sistema como um todo [(1)](requisitos.md#referencias). Assim, os requisitos garantem que o produto final atenda às expectativas e necessidades propostas inicialmente pelo cliente.

Abaixo estão os links para as listas de requisitos funcionais e requisitos não funcionais referentes a cada subsistema:

- [Requisitos de Eletrônica](./requisitos-eletronica.md)
- [Requisitos de Energia](./requisitos-energia.md)
- [Requisitos de Software](./requisitos-software.md)
- [Requisitos de Estrutura](./requisitos-estrutura.md)

## Referências

> 1. SOMMERVILLE, Ian. Software Engineering. 2011.

## Histórico de versões

| Versão | Alteração                                  | Data       | Autor                                   |
| ------ | ------------------------------------------ | ---------- | --------------------------------------- |
| 1.0    | Documentação da visão geral dos requisitos | 27/04/2024 | Lucas Gabriel, Kayro César e Luan Vasco |
