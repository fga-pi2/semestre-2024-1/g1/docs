# Video de demonstração do produto

Segue o video de demonstração e uso do produto final:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yhSpwNfcAMw?si=fAV36OyMuM3wGc_k" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

[Link para o Youtube](https://youtu.be/yhSpwNfcAMw)