## Introdução

Segundo a OMS, a dengue é uma doença viral que tem se espalhado rapidamente por diversas regiões do mundo e é transmitida por mosquitos fêmeas, sendo esses principalmente da espécie _Aedes aegypti_ e em uma proporção menor pela espécie _Aedes albopictus_. Além disso, tais mosquitos também são vetores de outras doenças como a chikungunya e a zika. Ademais, a dengue é prevalente nos trópicos, variando conforme fatores como precipitação, temperatura e rápida urbanização não planejada. [(1)](visaogeral.md#referencias)

Nesse contexto, o mosquito tem como condição de proliferação um ambiente possuindo água e parada para manter seu ciclo de desenvolvimento, demandando ações de intervenção para impedir sua proliferação por determinada região. [(7)](visaogeral.md#referencias). Concomitantemente, os casos de dengue têm tido aumentos expressivos em regiões como o Distrito Federal (DF), fazendo-se fulcral a implementação de políticas públicas com vistas a diminuição desse óbice. [(6)](visaogeral.md#referencias)

## Detalhamento do problema

Atualmente, há um combate a mosquitos e as doenças transmitidas por eles com um número elevado de entorno de 700 mil mortes por ano mundialmente. [(4)](visaogeral.md#referencias) Os mosquitos são verdadeiros especialistas na percepção do ambiente que os cercam, possuem sensores que os guiam até seus alvos, sensores esses de calor, gás carbônico e odores presente no suor. Além do mais, apresentam um formato e cores que os tornam muito difíceis de serem avistados nos ambientes urbanos e em florestas que normalmente vivem, tendo a capacidade de sobrevivência por até 450 dias sem água em seu estado oval. Além disso, aspectos como a urbanização, crescimento desordenado da população, saneamento básico deficitário e os fatores climáticos mantêm as condições favoráveis para a presença do _Aedes aegypti_, com reflexos na dinâmica de transmissão do vírus, principalmente no Brasil. [(2)](visaogeral.md#referencias)

Os problemas citados anteriormente tornam o combate à dengue difícil e oneroso. Nesse cenário, o governo então sempre assumiu o papel de criar planos e executá-los em relação ao combate não só a dengue como também a Zika e Febre Amarela. Porém, observa-se que a centralização desse combate tem produzido lacunas na execução, pois caso o governo em um determinado momento não tome as decisões adequadas e as condições sejam favoráveis como foi o caso do Distrito Federal no inicio de 2024, existe uma grande possibilidade de aumento de focos e consequentemente no número de casos de dengue.

Congruente a isso, o número de casos no Distrito Federal (DF) tem tido um aumento significativo no período de 31/12/2023 a 10/02/2024 em comparação ao mesmo período no ano anterior com um aumento de 1.303,9% no número de casos prováveis de dengue em residentes do DF. Nesse contexto, a dengue no DF ocorre principalmente de forma sazonal com maior intensidade entre os meses de outubro e maio. Portanto, tal aumento nos casos prováveis de dengue em residentes do DF pode ser observado no gráfico da imagem a seguir. [(6)](visaogeral.md#referencias)

<figcaption style="text-align: center">
    <b>Figura 1: Gráfico de casos prováveis de dengue por semana epidemiológica de início de sintomas no ano de 2023 e até o mês 06 de 2024</b>
</figcaption>

<center>

![Gráfico de casos prováveis de dengue por semana epidemiológica de início de sintomas no ano de 2023 e até o mês 06 de 2024](assets/images/grafico_casos_dengue.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: SINAN Online <a href="#referencias"> (6)</a></small>
</figcaption>

<figcaption style="text-align: center">
    <b>Tabela 1: Visão geral da solução proposta</b>
</figcaption>
<center>

Diante deste cenário, surge a necessidade de uma solução para combater a transmissão da dengue e mitigar seus impactos na saúde e na sociedade.

|                                  |                                                                                                                 |
| -------------------------------- | --------------------------------------------------------------------------------------------------------------- |
| **O problema**                   | Problema da alta taxa de dengue no país                                                                         |
| **Afeta**                        | Toda a população brasileira, em especial habitantes de regiões muito urbanizadas                                |
| **Cujo impacto é**               | Pessoas infectadas, óbitos, hiperlotação de hospitais e consequente crise no sistema público e privado de saúde |
| **Uma solução de sucesso seria** | Uma que diminuísse a transmissão da dengue e permitisse a identificação de focos do mosquito                    |

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Declaração da posição do produto

<figcaption style="text-align: center">
    <b>Tabela 2: Declaração da posição do produto</b>
</figcaption>
<center>

|                   |                                                                                                          |
| ----------------- | -------------------------------------------------------------------------------------------------------- |
| **Para**          | Pesquisadores ou inspetores de saúde                                                                     |
| **Quem**          | Precisam identificar, estudar e combater focos do mosquito da dengue                                     |
| **O Ovnitrap**    | é um produto composto por uma armadilha e um aplicativo móvel                                            |
| **Que**           | Detecta a posição geográfica de focos de dengue, tal como impede o desenvolvimento de larvas do mosquito |
| **Ao contrário**  | Ovitrampas                                                                                               |
| **Nosso produto** | Apresenta relatórios de atividade das armadilhas e mapas de calor de focos do mosquito da dengue         |

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Descrição dos _Stakeholders_

### 1. Pesquisadores

**Interesses e Expectativas:** Os pesquisadores esperam utilizar a armadilha para identificar mosquitos da região, utilizando a ferramenta de acesso às imagens dos relatórios, assim como a luz UV presente no aparelho para identificar os mosquitos geneticamente modificados e assim utilizar esses dados nos seus próprios produtos como o Aedes do bem ou artigos de pesquisas.

**Necessidades e Preocupações:** Eles precisam de um produto que seja fácil de implantar e manter, que forneça relatórios com dados para suas pesquisas.

**Como o Produto Pode Atender:** O Ovnitrap oferece relatórios sobre a atividade das armadilhas, permitindo que os pesquisadores identifiquem, acessem imagens e dados precisos da região em questão.

### 2. Inspetores de Saúde Local

**Interesses e Expectativas:** Os inspetores de saúde locais estão preocupados em garantir a segurança e o bem-estar da comunidade em sua área de atuação. Eles esperam que o produto os ajude a identificar e eliminar os focos do mosquito da dengue em tempo hábil.

**Necessidades e Preocupações:** Eles precisam de ferramentas que facilitem o monitoramento dos locais propensos à proliferação de mosquitos. Além disso, estão interessados em relatórios que os ajudem a tomar decisões informadas.

**Como o Produto Pode Atender:** O aplicativo móvel do Ovnitrap permite que os inspetores visualizem facilmente os dados das armadilhas em seus dispositivos, facilitando o monitoramento e a tomada de decisões. O produto também elimina os ovos e larvas contidos na armadilha de forma segura.

### 3. Autoridades de Saúde Pública

**Interesses e Expectativas:** As autoridades de saúde pública têm a responsabilidade de implementar políticas e programas de controle de doenças. Elas esperam que o produto contribua para a redução da transmissão da dengue e melhore a eficácia de suas intervenções.

**Necessidades e Preocupações:** Elas precisam de dados para avaliar a eficácia das medidas de controle de doenças e justificar alocações de recursos.

**Como o Produto Pode Atender:** O Ovnitrap fornece dados sobre a atividade dos mosquitos da dengue, permitindo que as autoridades de saúde tomem decisões baseadas em evidências e ajustem suas estratégias conforme necessário.

### 4. Comunidade Local

**Interesses e Expectativas:** A comunidade local está preocupada com sua própria segurança e saúde, bem como com a de seus entes queridos. Ela espera que o produto ajude a reduzir a incidência de doenças transmitidas por mosquitos em sua área.

**Necessidades e Preocupações:** Ela precisa de informações sobre a presença de mosquitos na área e tomar providências. Além disso, está interessada em medidas práticas de controle de mosquitos que possa implementar em sua própria casa.

**Como o Produto Pode Atender:** O Ovnitrap fornece dados sobre a atividade dos mosquitos da dengue, permitindo que os membros da comunidade tomem medidas responsivas para proteger suas famílias e vizinhos.

## Levantamento de normas técnicas relacionadas ao problema

- **IEC 62133** - Requisitos de segurança e desempenho para baterias recarregáveis: Estabelecimento de requisitos de segurança e desempenho para baterias recarregáveis, para que sejam manuseadas corretamente (IEC, 2017). Essa norma foi utilizada durante os procedimentos de carregamento das baterias no laboratório de eletrônica.

- **ABNT NBR ISO 9001:2015** - Sistema de Gestão da Qualidade: Esta norma
  estabelece requisitos para sistemas de gestão da qualidade e será relevante para
  garantir a qualidade e confiabilidade dos componentes do sistema (ABNT, 2015b).

- **ASTM E2877** - Termômetros digitais: Padrão e guia de utilização de termômetros
  digitais, assim como sensores de temperatura, para garantir a precisão das medições
  (ASTM, 2019). Essa norma foi utilizada na implementação do sensor de temperatura utilizado pelo dispositivo.

- **ISO/IEC 12207** - Processos de ciclo de vida de software: Esta norma estabelece
  atividades e tarefas associadas com ao ciclo de vida de um software, desde sua
  concepção até a descontinuação e servirá como base para a produção do software
  do sistema (SINGH, 1996).

- **NBR IEC 60529** - é uma norma aplicada para a classificação dos graus de proteção
  (IP) fornecidos aos invólucros dos equipamentos elétricos com tensão nominal não superior a 72,5 kV. Essa norma foi utilizada na forma de uma caixa de isopor que isolava os componentes eletrônicos do ambiente e dos elementos do calor e da água. 

- **ABNT 5410** - Que dita sobre Instalações elétricas de baixa tensão, e sobre dimensionamento de cabeamentos elétricos. Essa norma foi uitlizado para dimensionar os fios e evitar a sobrecarga de algum.

- **NR-6:** Norma que regulamenta a execução do trabalho com uso de Equipamentos de Proteção Individual (EPI), sem estar condicionada a setores ou atividades econômicas específicas. Norma utilizada para manuseio de ferramentas e impressoras 3D (GOV, 1978) e durante a produção das peças no laboratorio de mecânica da FGA, como no uso de óculos de proteção, luvas e roupas adequadas.

- **ABNT NBR ISO 11228-1:** Norma de ergonomia - Movimentação Manual. Norma que especifica os limites recomendados para o levantamento manual e transporte de cargas, levando em consideração a intensidade, a frequência e a duração da tarefa (NORMAS, 2017). Essa norma foi utilizada para observar o peso máximo e a forma de deslocamento do dispositivo uitilizando rodas para que não incorra em danos ao técnico responsável pela instalação.

## Identificação de soluções comerciais

As soluções alternativas ao problema já existem em grande variedade, dentre elas pode-se citar o uso de repelentes tópicos pessoais, a instalação de telas nas portas e janelas das casas, venenos químicos, reprodução em laboratório de mosquitos modificados ou infectados que impeçam a transmissão de doenças e combate por prevenção retirando pontos de reprodução do mosquito. Infelizmente essas soluções não tem apresentando um resultado satisfatório e existem algumas razões para isso. Sendo a principal o governo que muitas vezes falha em ter um plano organizado antes dos períodos de maior incidência dos mosquitos. O uso de soluções variadas tende a ser eficiente, mas deve ser impulsionado pelo governo.

A solução comercial que mais poderia competir com a apresentada neste trabalho é a "Aedes do Bem" que consiste em reproduzir mosquitos modificados e liberar no ambiente. Essa técnica é efetiva pois é possível reproduzir os mosquitos em fábricas especializadas e enviá-los por todo o país. A forma de ativar a ferramenta é apenas adicionar água e esperar os mosquitos se desenvolverem. Existe pouco esforço dos usuários e demonstrou grande êxito em Niterói que teve a menor taxa de incidência do estado do Rio de Janeiro. As desvantagens dessa técnica são o custo relativamente alto por armadilha de R$200,00 e a constante necessidade de estudar as espécies de mosquitos e adaptar de forma correta os mosquitos modificados. Sendo essa a solução mais eficiente vista até o momento, a solução proposta deveria pelo menos se aproximar da eficiência dela.

Abaixo é apresentado um comparativo resumido das soluções encontradas e da proposta.

<figcaption style="text-align: center">
    <b>Tabela 3: Comparativo das soluções existentes e da proposta</b>
</figcaption>
<center>

|                               | Armadilhas UV           | Aedes do Bem  | Inseticida (fumacê) | Ovnitrap             |
| :---------------------------: | ----------------------- | ------------- | ------------------- | -------------------- |
|      **Área de atuação**      | Pontual                 | 5km²          | 17km²               | Pontual              |
|           **Custo**           | R$100~R$300             | R$200         | R$400               | ~R$2000              |
|         **Segurança**         | Radiação UV no ambiente | Nenhum risco  | Químico             | Nenhum risco         |
|      **Dados coletados**      | Telemetria possível     | Nenhum        | Nenhum              | Telemetria e imagens |
| **Complexidade de aplicação** | baixa                   | baixa         | Alta                | Média                |
|     **Aceitação Pública**     | Boa                     | sem avaliação | Baixa               | Boa                  |

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

Para gerar essa tabela uma pesquisa foi feita a partir de um formulário que pode ser visto em [Resultados da Pesquisa sobre Alternativas para Combate à Dengue](apendice10.md)

Utilizando a tabela é possível fazer algumas observações:

1 - Apesar da área de atuação do fumacê ser bem maior que as demais, ele tem o pior meio de aplicação pois pode contaminar pessoas que estiverem na área com o que é essencialmente um pesticida ou veneno. Além de exigir uma equipe treinada para manusear os produtos químicos tendo a pior complexidade de uso.

2 - O preço da solução Ovnitrap aqui proposta é alto, porém esse é um preço atual para um protótipo, um produto final ainda dependeria de uma adaptação para produção em massa que poderia abaixar o preço, apesar disso o preço real ainda seria caro comparado a outras soluções.

3 - Os dispositivos que utilizam luz UV poderiam incorporar dados telemétricos para se equipararem ao projeto proposto, isso aumentaria o preço mas ainda podem competir dentro do orçamento de R$2.000,00.

## Objetivo Geral do Projeto

O projeto visa desenvolver um sistema automatizado e interdisciplinar que seja capaz de capturar, monitorar e eliminar as larvas do mosquito _Aedes aegypti_, possibilitando a identificação de focos, promovendo a redução dos índices de infestação e, consequentemente, minimizando o risco de transmissão da dengue.

## Objetivos do produto

Utilizando a metodologia SMART, podemos detalhar os objetivos específicos como:

**Específico:** A armadilha Ovnitrap deve ser capaz de trabalhar de forma independente em um prazo de no mínimo 7 dias, ser capaz de avisar em caso de defeitos. Identificar com no mínimo 80% de acerto que tem larvas no container.

**Mensurável:** Implementar funcionalidades de monitoramento da armadilha no aplicativo móvel, utilizando esses dados para comparar com o desempenho de outras armadilhas equivalentes.

**Atingível:** Utilizar recursos Open Source e/ou de baixo custo, incluindo as tecnologias para detecção de larvas, para envio e recebimento de dados, e para o desenvolvimento do aplicativo móvel, afim de criar uma solução viável e de fácil manutenção para o projeto. Utilizar o produto para ajudar pesquisadores utilizando os dados adquiridos.

**Relevante:** O produto visa abordar o problema da dengue, o qual vem afetando toda a população brasileira, especialmente habitantes de regiões muito urbanizadas. Ao oferecer uma solução inovadora, eficaz e com a geração de energia renovável, o Ovnitrap tem o potencial de reduzir a transmissão da dengue e salvar vidas. Fornecer dados pertinentes a pesquisadores da área.

**Temporalmente definido:** O produto deve ser desenvolvido e finalizado no período de tempo da disciplina de Projeto Integrador 2, sendo ela composta por 3 pontos de controle e uma entrega final.

## BPMN

O BPMN(Business Process Model Notation) é um diagrama que mapeia o modelo de negócio de um produto do início ao fim. [(5)](visaogeral.md#referencias) Para o projeto Ovnitrap, o BPMN desenvolvido foi o seguinte:

<figcaption style="text-align: center">
    <b>Figura 2 - BPMN</b>
</figcaption>

<center>

![BPMN](assets/images/BPMN.png)

</center>

<figcaption style="text-align: center">
    <small>Fonte: Elaboração própria</small>
</figcaption>

## Referências

> 1. PAN AMERICAN HEALTH ORGANIZATION (PAHO). Dengue. Disponível em: https://www.paho.org/pt/topicos/dengue. Acesso em: 25 de abril de 2024.

> 2. MINISTÉRIO DA SAÚDE (MS). Dengue. Disponível em: https://www.gov.br/saude/pt-br/assuntos/saude-de-a-a-z/d/dengue . Acesso em: 15 de abril de 2024.

> 3. Secretaria de Estado de Saúde do Rio de Janeiro (SES). Dashboard da Dengue. Disponível em: https://sistemas.saude.rj.gov.br/tabnetbd/dash/dash_dengue.htm. Acesso em: 10 de abril de 2024.

> 4. PFIZER. 6 mosquito diseases that can be deadly. Disponível em: https://www.pfizer.com/news/articles/6_mosquito_diseases_that_can_be_deadly#.Yweo2_DM88I.link. Acesso em: 24 de abril de 2024.

> 5. BPMN. Business Process Model Notation. Disponível em: https://www.bpmn.org/ Acesso em : 28 de abril de 2024.

> 6. Monitoramento dos casos de dengue até a Semana Epidemiológica 06 de 2024 no Distrito Federal. Disponível em: <https://www.saude.df.gov.br/documents/37101/0/06_BOLETIM_SEMANAL_DENGUE_SE_06_+DF+2024.pdf/f4f0ca52-82f0-a505-264f-fe7052f5cfe9?t=1707944555411#:~:text=Em%202024%2C%20at%C3%A9%20a%20SE>. Acesso em: 22 de maio de 2024.

> 7. PRODEST ; MOSQUITO. Ações da população são importantes para evitar a proliferação do Aedes aegypti. mosquito. Disponível em: <https://mosquito.saude.es.gov.br/Not%C3%ADcia/acoes-da-populacao-sao-importantes-para-evitar-a-proliferacao-do-aedes-aegypti#:~:text=Ciclo%20de%20reprodu%C3%A7%C3%A3o%20do%20mosquito,se%20transformam%20em%20mosquitos%20adultos.>. Acesso em: 31 de maio de 2024.

‌

## Histórico de versões

| Versão | Alteração                                                                             | Data       | Autor                                     |
| ------ | ------------------------------------------------------------------------------------- | ---------- | ----------------------------------------- |
| 1.0    | Criação do documento                                                                  | 24/04/2024 | Luís Guilherme G. Lins                    |
| 1.1    | Acréscimo de tópicos declaração do problema, levantamento de normas técnicas e outros | 25/04/2024 | Gabriel Bacon e Jhéssica Isabel           |
| 1.2    | Revisão do documento                                                                  | 24/04/2024 | Kayro César e Luan Vasco                  |
| 1.3    | Acréscimo normas técnicas de estrutura                                                | 27/04/2024 | Carlos Vaz                                |
| 1.4    | Lista Faz/Não Faz e É/não É                                                           | 28/04/2024 | Todos os integrantes do grupo via discord |
| 1.5    | Acréscimo do BPMN                                                                     | 28/04/2024 | Luan Vasco, Lucas, Kayro                  |
| 1.6    | Revisão semântica e ortográfica                                                       | 29/04/2024 | Gabriel e Luís Lins                       |
| 1.7    | Adição de contexto sobre a dengue no DF                                               | 22/05/2024 | Lucas Gabriel                             |
| 1.8    | Correções gerais no documento                                                         | 31/05/2024 | Lucas Gabriel                             |
| 1.9    | adicionar link para apêndice 10                                                       | 31/05/2024 | Victor Yukio                              |
| 2.0    | Revisão e atualizações                                                                | 03/06/2024 | Gabriel Bacon                             |
| 2.1    | Revisão e atualizações                                                                | 12/07/2024 | Gabriel Bacon                             |

