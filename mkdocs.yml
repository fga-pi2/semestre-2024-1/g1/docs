site_name: Ovnitrap
repo_name: Ovnitrap
repo_url: "https://gitlab.com/fga-pi2/semestre-2024-1/g1/docs"
site_url: https://ovnitrap.gitlab.io/docs/

theme:
  name: material
  logo: assets/images/logo.png
  favicon: assets/images/logo.png
  features:
    - navigation.tabs
    - toc.integrate
    - content.tabs.link
    - navigation.footer
  palette:
    - scheme: default
      primary: white
      accent: light blue
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode

    - scheme: slate
      primary: black
      accent: light blue
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
  extra:
    social:
      - icon: fontawesome/brands/gitlab
        link: https://gitlab.com/fga-pi2/semestre-2024-1/G1/OvinTrap

extra_css:
  - styles/extra.css

markdown_extensions:
  - md_in_html
  - admonition
  - pymdownx.details
  - pymdownx.critic
  - attr_list
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format

nav:
  - "Sobre": "index.md"
  - "Visão Geral": "visaogeral.md"
  - "Problematização":
    - "Visão geral dos Requisitos": "requisitos.md"
    - "Requisitos de Eletrônica": "requisitos-eletronica.md"
    - "Requisitos de Energia": "requisitos-energia.md"
    - "Requisitos de Software": "requisitos-software.md"
    - "Requisitos de Estrutura": "requisitos-estrutura.md"
    
  - "Detalhamento da Solução":
    - "Arquitetura do Subsistema de Eletrônica": "arquitetura-eletronica.md"
    - "Arquitetura do Subsistema de Energia": "arquitetura-energia.md"
    - "Arquitetura do Subsistema de Software": "arquitetura-software.md"
    - "Arquitetura do Subsistema de Estrutura": "arquitetura-estrutura.md"

  - "Projeto e Subsistemas":
    - "Projeto de Eletrônica": "projeto-eletronica.md"
    - "Projeto de Energia": "projeto-energia.md"
    - "Projeto de Software": "projeto-software.md"
    - "Projeto de Estrutura": "projeto-estrutura.md"

  - "Integração e Finalização":
    - "Visão Geral da Execução": "execucao.md"
    - "Eletrônica": "execucao-eletronica.md"
    - "Energia": "execucao-energia.md"
    - "Software": "execucao-software.md"
    - "Estrutura": "execucao-estrutura.md"
  
  - "Apêndices":
    - "1. Aspectos de gerenciamento do projeto": "apendice1.md"
    - "2. Diagramas de Eletrônica": "apendice2.md"
    - "3. Diagrama de casos de uso": "apendice3.md"
    - "4. Orçamento do projeto": "apendice4.md"
    - "5. Estrutura Analítica do Projeto": "apendice5.md"
    - "6. Esquemático de energia": "apendice6.md"
    - "7. Desenho Técnico dos Componentes da Estrutura": "apendice7.md"
    - "8. Gerenciamento de Riscos do Projeto": "apendice8.md"
    - "9. Ferramentas utilizadas para o desenvolvimento do software": "apendice9.md"
    - "10. Formulário de pesquisa de combate a dengue": "apendice10.md"
    - "11. Banner de apresentação": "apendice11.md"
    
  - "Video": "video.md"
